package com.yunbao.main.utils.broccoli;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;

class PlaceholderPreStateSaver {
    private ColorStateList restoredTextColor;
    private Drawable restoredTextLeftDrawable;
    private Drawable restoredTextTopDrawable;
    private Drawable restoredTextRightDrawable;
    private Drawable restoredTextBottomDrawable;

    private Drawable restoredImageDrawable;

    private Drawable restoredBackgroundDrawable;

    public PlaceholderPreStateSaver(){

    }

    protected ColorStateList getRestoredTextColor() {
        return restoredTextColor;
    }

    protected void setRestoredTextColor(ColorStateList restoredTextColor) {
        this.restoredTextColor = restoredTextColor;
    }

    protected Drawable getRestoredTextLeftDrawable() {
        return restoredTextLeftDrawable;
    }

    protected void setRestoredTextLeftDrawable(Drawable restoredTextLeftDrawable) {
        this.restoredTextLeftDrawable = restoredTextLeftDrawable;
    }

    protected Drawable getRestoredTextTopDrawable() {
        return restoredTextTopDrawable;
    }

    protected void setRestoredTextTopDrawable(Drawable restoredTextTopDrawable) {
        this.restoredTextTopDrawable = restoredTextTopDrawable;
    }

    protected Drawable getRestoredTextRightDrawable() {
        return restoredTextRightDrawable;
    }

    protected void setRestoredTextRightDrawable(Drawable restoredTextRightDrawable) {
        this.restoredTextRightDrawable = restoredTextRightDrawable;
    }

    protected Drawable getRestoredTextBottomDrawable() {
        return restoredTextBottomDrawable;
    }

    protected void setRestoredTextBottomDrawable(Drawable restoredTextBottomDrawable) {
        this.restoredTextBottomDrawable = restoredTextBottomDrawable;
    }

    public Drawable getRestoredImageDrawable() {
        return restoredImageDrawable;
    }

    public void setRestoredImageDrawable(Drawable restoredImageDrawable) {
        this.restoredImageDrawable = restoredImageDrawable;
    }

    public Drawable getRestoredBackgroundDrawable() {
        return restoredBackgroundDrawable;
    }

    public void setRestoredBackgroundDrawable(Drawable restoredBackgroundDrawable) {
        this.restoredBackgroundDrawable = restoredBackgroundDrawable;
    }
}
