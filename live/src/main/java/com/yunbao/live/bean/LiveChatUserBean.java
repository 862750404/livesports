package com.yunbao.live.bean;

public class LiveChatUserBean {
    /**
     * user_nicename : 游客1117
     * uid :
     */

    private String user_nicename;
    private String uid;
    private String touid;
    private String tousername;

    public String getUser_nicename() {
        return user_nicename;
    }

    public void setUser_nicename(String user_nicename) {
        this.user_nicename = user_nicename;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTouid() {
        return touid;
    }

    public void setTouid(String touid) {
        this.touid = touid;
    }

    public String getTousername() {
        return tousername;
    }

    public void setTousername(String tousername) {
        this.tousername = tousername;
    }
}
