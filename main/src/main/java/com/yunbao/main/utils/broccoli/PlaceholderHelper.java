package com.yunbao.main.utils.broccoli;
import android.graphics.Color;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import com.yunbao.main.R;

public class PlaceholderHelper {
    private PlaceholderHelper() {
        throw new UnsupportedOperationException("Can not be instantiated.");
    }

    public static PlaceholderParameter getParameter(View view) {
        if (view == null) {
            return null;
        }
        int placeHolderColor = Color.parseColor("#dddddd");

        int id = view.getId();
        if (id == R.id.tv_item_home_game_title) {
            return new PlaceholderParameter.Builder()
                    .setView(view)
                    .setDrawable(DrawableUtils.createRectangleDrawable(placeHolderColor, 0))
                    .build();
        } else if (id == R.id.tv_game_status) {
            return new PlaceholderParameter.Builder()
                    .setView(view)
                    .setDrawable(DrawableUtils.createRectangleDrawable(placeHolderColor, 0))
                    .build();
        } else if (id == R.id.iv_left) {
            return new PlaceholderParameter.Builder()
                    .setView(view)
                    .setDrawable(DrawableUtils.createRectangleDrawable(placeHolderColor, 5))
                    .build();
        } else if (id == R.id.tv_game_result) {
            return new PlaceholderParameter.Builder()
                    .setView(view)
                    .setDrawable(DrawableUtils.createOvalDrawable(placeHolderColor))
                    .build();
        } else if (id == R.id.iv_right) {
            Animation timeAnimation = new ScaleAnimation(0.3f, 1, 1, 1);
            timeAnimation.setDuration(600);
            timeAnimation.setRepeatMode(Animation.REVERSE);
            timeAnimation.setRepeatCount(Animation.INFINITE);
            return new PlaceholderParameter.Builder()
                    .setView(view)
                    .setAnimation(timeAnimation)
                    .setDrawable(DrawableUtils.createRectangleDrawable(placeHolderColor, 5))
                    .build();
        } else if (id == R.id.iv_right) {
            return new PlaceholderParameter.Builder()
                    .setView(view)
                    .setDrawable(DrawableUtils.createOvalDrawable(placeHolderColor))
                    .build();
        } else if (id == R.id.tv_item_home_game_hint) {
            return new PlaceholderParameter.Builder()
                    .setView(view)
                    .setDrawable(DrawableUtils.createOvalDrawable(placeHolderColor))
                    .build();
        }

        return null;
    }
}