package com.yunbao.main.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.donkingliang.groupedadapter.adapter.GroupedRecyclerViewAdapter;
import com.donkingliang.groupedadapter.holder.BaseViewHolder;
import com.yunbao.common.Constants;
import com.yunbao.common.bean.GameFootballAllListBean;
import com.yunbao.common.bean.GameFootballMatchBean;
import com.yunbao.common.utils.AppLog;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.common.utils.TimeUtil;
import com.yunbao.live.activity.LiveSportsActivity;
import com.yunbao.main.R;

import java.util.ArrayList;
import java.util.List;


public class MainGameFootballAllListAdapter extends GroupedRecyclerViewAdapter {

    private Context mContext;
    private List<GameFootballAllListBean> gameFootballAllListBeanList;
    private RequestOptions options = new RequestOptions()
            .placeholder(R.mipmap.icon_default_logo)
            .error(R.mipmap.icon_default_logo);

    public MainGameFootballAllListAdapter(Context context, ArrayList<GameFootballAllListBean> groups) {
        super(context);
        this.mContext = context;
        if (groups == null) {
            gameFootballAllListBeanList = new ArrayList<>();
        } else {
            gameFootballAllListBeanList = groups;
        }
        this.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public void onChildClick(GroupedRecyclerViewAdapter adapter, BaseViewHolder holder, int groupPosition, int childPosition) {
                if (!ClickUtil.canClick()) return;
                Intent intent = new Intent(mContext, LiveSportsActivity.class);
                intent.putExtra(Constants.LIVE_MATCH_FOOTBALL_INFO, gameFootballAllListBeanList.get(groupPosition).getList().get(childPosition));
                mContext.startActivity(intent);
            }
        });
    }

    public void clear() {
        if (gameFootballAllListBeanList != null) {
            gameFootballAllListBeanList.clear();
        }
        notifyDataChanged();
    }

    public void setGroups(List<GameFootballAllListBean> groups) {
        if (gameFootballAllListBeanList != null) {
            this.gameFootballAllListBeanList = groups;
            notifyDataChanged();
        }
    }

    public void addGroups(List<GameFootballAllListBean> groups) {
        if (this.gameFootballAllListBeanList != null && groups != null && !groups.isEmpty()) {
            for (int i = 0; i < groups.size(); i++) {
                if (groups.get(i).getWeek_day_str().equals(gameFootballAllListBeanList.get(gameFootballAllListBeanList.size() - 1).getWeek_day_str())) {
                    gameFootballAllListBeanList.get(gameFootballAllListBeanList.size() - 1).getList().addAll(groups.get(i).getList());
                } else {
                    gameFootballAllListBeanList.add(groups.get(i));
                }
            }
            notifyDataChanged();
        }
    }

    @Override
    public int getGroupCount() {
        //返回组的数量
        return gameFootballAllListBeanList == null ? 0 : gameFootballAllListBeanList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        //返回当前组的子项数量
        return gameFootballAllListBeanList == null ? 0 : gameFootballAllListBeanList.get(groupPosition).getList().size();
    }

    @Override
    public boolean hasHeader(int groupPosition) {
        //当前组是否有头部
        return true;
    }

    @Override
    public boolean hasFooter(int groupPosition) {
        //当前组是否有尾部
        return false;
    }

    @Override
    public int getHeaderLayout(int viewType) {
        //返回头部的布局id。(如果hasHeader返回false，这个方法不会执行)
        return R.layout.layout_sticky_header_view;
    }

    @Override
    public int getFooterLayout(int viewType) {
        //返回尾部的布局id。(如果hasFooter返回false，这个方法不会执行)
        return R.layout.layout_sticky_header_view;
    }

    @Override
    public int getChildLayout(int viewType) {
        //返回子项的布局id。
        return R.layout.layout_game_football_list_item;
    }

    @Override
    public void onBindHeaderViewHolder(BaseViewHolder holder, int groupPosition) {
        //绑定头部布局数据。(如果hasHeader返回false，这个方法不会执行)
        GameFootballAllListBean gameFootballMatchBean = gameFootballAllListBeanList.get(groupPosition);
        holder.setText(R.id.tv_sticky_header_view, gameFootballMatchBean.getWeek_day_str());
    }

    @Override
    public void onBindFooterViewHolder(BaseViewHolder holder, int groupPosition) {
        //绑定尾部布局数据。(如果hasFooter返回false，这个方法不会执行)
//        GameFootballAllListBean gameFootballMatchBean = gameFootballAllListBeanList.get(groupPosition);
//        holder.setText(R.id.tv_footer, gameFootballMatchBean.getFooter());
    }

    @Override
    public void onBindChildViewHolder(BaseViewHolder holder, int groupPosition, int childPosition) {
        //绑定子项布局数据。
        try {
            GameFootballMatchBean gameFootballMatchBean = gameFootballAllListBeanList.get(groupPosition).getList().get(childPosition);

            holder.setText(R.id.tv_time, TimeUtil.stampToTimeMM(gameFootballMatchBean.getMatchStartTime() + "000"));
            holder.setText(R.id.tv_game_name, gameFootballMatchBean.getLeague() == null ? "" : gameFootballMatchBean.getLeague().getLeagueNameCnShort());
//          tv_game_wheel.setText(gameFootBallMatchBean.get);
            holder.setText(R.id.tv_a_score, TextUtils.isEmpty(gameFootballMatchBean.getHomeScore()) ? "0" : gameFootballMatchBean.getHomeScore());
            holder.setText(R.id.tv_b_score, TextUtils.isEmpty(gameFootballMatchBean.getAwayScore()) ? "0" : gameFootballMatchBean.getAwayScore());
//          1未开赛，2比赛中，3已完赛
            if (gameFootballMatchBean.isIs_playing() == 2) {
                holder.setText(R.id.tv_game_des, gameFootballMatchBean.getState_str());
                holder.setTextColor(R.id.tv_game_des, mContext.getResources().getColor(R.color.red1));
                holder.setBackgroundRes(R.id.ll_bg_play_status, R.drawable.bg_shape_rounded_corners_red_normal);
                holder.setText(R.id.tv_play_status, R.string.is_playing);
                holder.setTextColor(R.id.tv_game_name, mContext.getResources().getColor(R.color.red1));
                holder.setImageResource(R.id.iv_play_status, R.mipmap.icon_ball_playing);
                holder.setImageResource(R.id.iv_ball, R.mipmap.icon_football_ing);
                holder.setTextColor(R.id.tv_a_score, mContext.getResources().getColor(R.color.red1));
                holder.setTextColor(R.id.tv_b_score, mContext.getResources().getColor(R.color.red1));
            } else if (gameFootballMatchBean.isIs_playing() == 3) {
                holder.setText(R.id.tv_game_des, "-");
                holder.setTextColor(R.id.tv_game_des, mContext.getResources().getColor(R.color.black2));
                holder.setImageResource(R.id.iv_ball, R.mipmap.icon_football_gray);
                holder.setTextColor(R.id.tv_a_score, mContext.getResources().getColor(R.color.gray1));
                holder.setTextColor(R.id.tv_b_score, mContext.getResources().getColor(R.color.gray1));
                holder.setBackgroundRes(R.id.ll_bg_play_status, R.drawable.bg_shape_rounded_corners_gray_light);
                holder.setText(R.id.tv_play_status, R.string.game_over);
                holder.setTextColor(R.id.tv_game_name, mContext.getResources().getColor(R.color.black2));
                holder.setImageResource(R.id.iv_play_status, R.mipmap.icon_ball_over);
            } else {
                holder.setText(R.id.tv_game_des, "-");
                holder.setTextColor(R.id.tv_game_des, mContext.getResources().getColor(R.color.black2));
                holder.setImageResource(R.id.iv_ball, R.mipmap.icon_football_gray);
                holder.setTextColor(R.id.tv_a_score, mContext.getResources().getColor(R.color.gray1));
                holder.setTextColor(R.id.tv_b_score, mContext.getResources().getColor(R.color.gray1));
                holder.setBackgroundRes(R.id.ll_bg_play_status, R.drawable.bg_shape_rounded_corners_gray);
                holder.setText(R.id.tv_play_status, R.string.game_not_start);
                holder.setTextColor(R.id.tv_game_name, mContext.getResources().getColor(R.color.black2));
                holder.setImageResource(R.id.iv_play_status, R.mipmap.icon_ball_not_start);
            }

            GameFootballMatchBean.HomeTeamBean home_team = gameFootballMatchBean.getHome_team();
            GameFootballMatchBean.AwayTeamBean away_team = gameFootballMatchBean.getAway_team();
            holder.setText(R.id.tv_team_a_name, home_team == null ? "" : home_team.getNameCn());
            holder.setText(R.id.tv_team_b_name, away_team == null ? "" : away_team.getNameCn());
            ImageView viewA = holder.get(R.id.iv_team_a);
            ImageView viewB = holder.get(R.id.iv_team_b);
            Glide.with(mContext)
                    .load(home_team == null ? R.mipmap.icon_default_logo : home_team.getLogo())
                    .apply(options)
                    .into(viewA);
            Glide.with(mContext)
                    .load(away_team == null ? R.mipmap.icon_default_logo : away_team.getLogo())
                    .apply(options)
                    .into(viewB);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * socket刷新数据
     *
     * @param gameFootballMatchBean
     */
    public void setUpdateData(List<Object> gameFootballMatchBean) {
        try {
            if (gameFootballMatchBean != null && !gameFootballMatchBean.isEmpty()
                    && gameFootballAllListBeanList != null && !gameFootballAllListBeanList.isEmpty() && getGroupCount() > 0) {
                AppLog.e("赛事列表收到通知：刷新足球全部列表");
                for (int i = 0; i < gameFootballAllListBeanList.size(); i++) {
                    for (int j = 0; j < gameFootballAllListBeanList.get(i).getList().size(); j++) {
                        for (int k = 0; k < gameFootballMatchBean.size(); k++) {
                            GameFootballMatchBean gameFootballMatch = (GameFootballMatchBean) gameFootballMatchBean.get(k);
                            if (gameFootballMatch.getMatchId().equals(gameFootballAllListBeanList.get(i).getList().get(j).getMatchId())) {
                                AppLog.e("全部足球有相同的id：" + gameFootballMatch.getMatchId());
                                gameFootballAllListBeanList.get(i).getList().get(j).setIs_playing(gameFootballMatch.isIs_playing());
                                gameFootballAllListBeanList.get(i).getList().get(j).setHomeScore(gameFootballMatch.getHomeScore());
                                gameFootballAllListBeanList.get(i).getList().get(j).setAwayScore(gameFootballMatch.getAwayScore());
                            }
                        }
                    }
                }
                notifyDataChanged();
            }
        } catch (Exception e) {
            AppLog.e(e.toString());
        }
    }
}