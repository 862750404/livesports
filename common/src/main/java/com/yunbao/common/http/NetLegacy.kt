package com.yunbao.common.http

import android.app.Application
import com.alibaba.fastjson.JSONObject
import com.drake.net.Get
import com.drake.net.NetConfig
import com.drake.net.initNet
import com.drake.net.transform.transform
import com.drake.net.utils.fastest
import com.drake.net.utils.scopeNet
import com.yunbao.common.CommonAppConfig
import com.yunbao.common.bean.ConfigBean
import com.yunbao.common.interfaces.CommonCallback
import com.yunbao.common.utils.SpUtil
import com.yunbao.common.utils.WordFilterUtil
import java.util.concurrent.TimeUnit

object NetLegacy {

    @JvmStatic
    fun initNet(app: Application) {
        app.initNet("") {
            converter(NetConvert())
            connectionTimeout(1, TimeUnit.MINUTES)
            readTimeout(1, TimeUnit.MINUTES)
        }
    }

    @JvmStatic
    fun getConfig(callback: CommonCallback<ConfigBean>) {
        scopeNet {
            val taskList = CommonAppConfig.HOSTS.map { host ->
                Get<ConfigBean>("${host}/?service=Home.getConfig").transform {
                    NetConfig.host = host
                    CommonAppConfig.setHost(host)
                    it
                }
            }
            val result = fastest(taskList, 0)
            CommonAppConfig.getInstance().config = result
            CommonAppConfig.getInstance().setLevel(JSONObject.toJSONString(result.level))
            CommonAppConfig.getInstance().setAnchorLevel(JSONObject.toJSONString(result.levelanchor))
            SpUtil.getInstance().setStringValue(SpUtil.CONFIG, JSONObject.toJSONString(result))
            WordFilterUtil.getInstance().initWordMap(result.sensitive_words)
            callback.callback(result)
        }.finally {
            if (it != null) callback.callback(null)
        }
    }
}