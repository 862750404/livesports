package com.yunbao.live.bean;


public class LiveWaterMarkBean {
    /**
     * position : 1
     * xpos : 2
     * ypos : 2
     * width : 9
     * height : 9
     * image : http://img.zbitcLoud.com/admin/20201023/65b92ae8e4c07a402c5683e3389d600e.jpg
     */

    private int position;
    private String xpos;
    private String ypos;
    private String width;
    private String height;
    private String image;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getXpos() {
        return xpos;
    }

    public void setXpos(String xpos) {
        this.xpos = xpos;
    }

    public String getYpos() {
        return ypos;
    }

    public void setYpos(String ypos) {
        this.ypos = ypos;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
