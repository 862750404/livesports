package com.yunbao.live.bean;

/**
 * @author wangjiang
 * @date 2020/9/24
 * Description:
 */
public class MultiGroupHistogramChildData {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
