package com.yunbao.main.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.donkingliang.groupedadapter.adapter.GroupedRecyclerViewAdapter;
import com.donkingliang.groupedadapter.holder.BaseViewHolder;
import com.yunbao.common.Constants;
import com.yunbao.common.bean.GameBasketballAllListBean;
import com.yunbao.common.bean.GameBasketballMatchBean;
import com.yunbao.common.bean.GameFootballMatchBean;
import com.yunbao.common.utils.AppLog;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.common.utils.TimeUtil;
import com.yunbao.live.activity.LiveSportsActivity;
import com.yunbao.main.R;

import java.util.ArrayList;
import java.util.List;


public class MainGameBasketballAllListAdapter extends GroupedRecyclerViewAdapter {

    private Context mContext;
    private List<GameBasketballAllListBean> gameBasketBallMatchBeanList;
    private RequestOptions options = new RequestOptions()
            .placeholder(R.mipmap.icon_default_logo)
            .error(R.mipmap.icon_default_logo);

    public MainGameBasketballAllListAdapter(Context context, ArrayList<GameBasketballAllListBean> groups) {
        super(context);
        if (groups == null) {
            gameBasketBallMatchBeanList = new ArrayList<>();
        } else {
            gameBasketBallMatchBeanList = groups;
        }
        this.mContext = context;
        this.setOnChildClickListener(new GroupedRecyclerViewAdapter.OnChildClickListener() {
            @Override
            public void onChildClick(GroupedRecyclerViewAdapter adapter, BaseViewHolder holder, int groupPosition, int childPosition) {
                if (!ClickUtil.canClick()) return;
                Intent intent = new Intent(mContext, LiveSportsActivity.class);
                intent.putExtra(Constants.LIVE_MATCH_BASKETBALL_INFO, gameBasketBallMatchBeanList.get(groupPosition).getList().get(childPosition));
                mContext.startActivity(intent);
            }
        });
    }

    public void clear() {
        if (gameBasketBallMatchBeanList != null) {
            gameBasketBallMatchBeanList.clear();
        }
        notifyDataChanged();
    }

    public void setGroups(List<GameBasketballAllListBean> groups) {
        if (gameBasketBallMatchBeanList != null) {
            this.gameBasketBallMatchBeanList = groups;
            notifyDataChanged();
        }
    }

    public void addGroups(List<GameBasketballAllListBean> groups) {
        if (this.gameBasketBallMatchBeanList != null && groups != null && !groups.isEmpty()) {
            for (int i = 0; i < groups.size(); i++) {
                if (groups.get(i).getWeek_day_str().equals(gameBasketBallMatchBeanList.get(gameBasketBallMatchBeanList.size() - 1).getWeek_day_str())) {
                    gameBasketBallMatchBeanList.get(gameBasketBallMatchBeanList.size() - 1).getList().addAll(groups.get(i).getList());
                } else {
                    gameBasketBallMatchBeanList.add(groups.get(i));
                }
            }
            notifyDataChanged();
        }
    }

    @Override
    public int getGroupCount() {
        //返回组的数量
        return gameBasketBallMatchBeanList == null ? 0 : gameBasketBallMatchBeanList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        //返回当前组的子项数量
        return gameBasketBallMatchBeanList == null ? 0 : gameBasketBallMatchBeanList.get(groupPosition).getList().size();
    }

    @Override
    public boolean hasHeader(int groupPosition) {
        //当前组是否有头部
        return true;
    }

    @Override
    public boolean hasFooter(int groupPosition) {
        //当前组是否有尾部
        return false;
    }

    @Override
    public int getHeaderLayout(int viewType) {
        //返回头部的布局id。(如果hasHeader返回false，这个方法不会执行)
        return R.layout.layout_sticky_header_view;
    }

    @Override
    public int getFooterLayout(int viewType) {
        //返回尾部的布局id。(如果hasFooter返回false，这个方法不会执行)
        return R.layout.layout_sticky_header_view;
    }

    @Override
    public int getChildLayout(int viewType) {
        //返回子项的布局id。
        return R.layout.layout_game_basketball_list_item;
    }

    @Override
    public void onBindHeaderViewHolder(BaseViewHolder holder, int groupPosition) {
        //绑定头部布局数据。(如果hasHeader返回false，这个方法不会执行)
        GameBasketballAllListBean gameBasketballAllListBean = gameBasketBallMatchBeanList.get(groupPosition);
        holder.setText(R.id.tv_sticky_header_view, gameBasketballAllListBean.getWeek_day_str());
    }

    @Override
    public void onBindFooterViewHolder(BaseViewHolder holder, int groupPosition) {
        //绑定尾部布局数据。(如果hasFooter返回false，这个方法不会执行)
        GameBasketballAllListBean gameBasketballAllListBean = gameBasketBallMatchBeanList.get(groupPosition);
//        holder.setText(R.id.tv_footer, gameFootballMatchBean.getFooter());
    }

    @Override
    public void onBindChildViewHolder(BaseViewHolder holder, int groupPosition, int childPosition) {
        //绑定子项布局数据。
        try {
            GameBasketballMatchBean gameBasketballAllListBean = gameBasketBallMatchBeanList.get(groupPosition).getList().get(childPosition);
            holder.setText(R.id.tv_time, TimeUtil.stampToTimeMM(gameBasketballAllListBean.getMatchStartTime() + "000"));
            holder.setText(R.id.tv_game_name, gameBasketballAllListBean.getLeague() == null ? "" : gameBasketballAllListBean.getLeague().getLeagueNameCnShort());
//          tv_game_wheel.setText(gameBasketballAllListBean.get);
            holder.setText(R.id.tv_a_score, TextUtils.isEmpty(gameBasketballAllListBean.getHomeScore()) ? "0" : gameBasketballAllListBean.getHomeScore());
            holder.setText(R.id.tv_b_score, TextUtils.isEmpty(gameBasketballAllListBean.getAwayScore()) ? "0" : gameBasketballAllListBean.getAwayScore());
//          1未开赛，2比赛中，3已完赛
            if (gameBasketballAllListBean.isIs_playing() == 2) {
                int matchConductTime = gameBasketballAllListBean.getMatchConductTime();
                holder.setText(R.id.tv_game_des, gameBasketballAllListBean.getState_str() + (matchConductTime / 60) + ":" + (matchConductTime % 60));
                holder.setTextColor(R.id.tv_game_des, mContext.getResources().getColor(R.color.red1));
                holder.setBackgroundRes(R.id.ll_bg_play_status, R.drawable.bg_shape_rounded_corners_red_normal);
                holder.setText(R.id.tv_play_status, R.string.is_playing);
                holder.setTextColor(R.id.tv_game_name, mContext.getResources().getColor(R.color.red1));
                holder.setImageResource(R.id.iv_play_status, R.mipmap.icon_ball_playing);
                holder.setImageResource(R.id.iv_ball, R.mipmap.icon_football_ing);
                holder.setTextColor(R.id.tv_a_score, mContext.getResources().getColor(R.color.red1));
                holder.setTextColor(R.id.tv_b_score, mContext.getResources().getColor(R.color.red1));
            } else if (gameBasketballAllListBean.isIs_playing() == 3) {
                holder.setText(R.id.tv_game_des, "-");
                holder.setTextColor(R.id.tv_game_des, mContext.getResources().getColor(R.color.black2));
                holder.setImageResource(R.id.iv_ball, R.mipmap.icon_football_gray);
                holder.setTextColor(R.id.tv_a_score, mContext.getResources().getColor(R.color.gray1));
                holder.setTextColor(R.id.tv_b_score, mContext.getResources().getColor(R.color.gray1));
                holder.setBackgroundRes(R.id.ll_bg_play_status, R.drawable.bg_shape_rounded_corners_gray_light);
                holder.setText(R.id.tv_play_status, R.string.game_over);
                holder.setTextColor(R.id.tv_game_name, mContext.getResources().getColor(R.color.black2));
                holder.setImageResource(R.id.iv_play_status, R.mipmap.icon_ball_over);
            } else {
                holder.setText(R.id.tv_game_des, "-");
                holder.setTextColor(R.id.tv_game_des, mContext.getResources().getColor(R.color.black2));
                holder.setImageResource(R.id.iv_ball, R.mipmap.icon_football_gray);
                holder.setTextColor(R.id.tv_a_score, mContext.getResources().getColor(R.color.gray1));
                holder.setTextColor(R.id.tv_b_score, mContext.getResources().getColor(R.color.gray1));
                holder.setBackgroundRes(R.id.ll_bg_play_status, R.drawable.bg_shape_rounded_corners_gray);
                holder.setText(R.id.tv_play_status, R.string.game_not_start);
                holder.setTextColor(R.id.tv_game_name, mContext.getResources().getColor(R.color.black2));
                holder.setImageResource(R.id.iv_play_status, R.mipmap.icon_ball_not_start);
            }

            GameBasketballMatchBean.HomeTeamBean home_team = gameBasketballAllListBean.getHome_team();
            GameBasketballMatchBean.AwayTeamBean away_team = gameBasketballAllListBean.getAway_team();
            holder.setText(R.id.tv_team_a_name, home_team == null ? "" : home_team.getNameCn());
            holder.setText(R.id.tv_team_b_name, away_team == null ? "" : away_team.getNameCn());
            ImageView viewA = holder.get(R.id.iv_team_a);
            ImageView viewB = holder.get(R.id.iv_team_b);
            Glide.with(mContext)
                    .load(home_team == null ? R.mipmap.icon_default_logo : home_team.getLogo())
                    .apply(options)
                    .into(viewA);
            Glide.with(mContext)
                    .load(away_team == null ? R.mipmap.icon_default_logo : away_team.getLogo())
                    .apply(options)
                    .into(viewB);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * socket刷新数据
     *
     * @param gameBasketballMatchBean
     */
    public void setUpdateData(List<Object> gameBasketballMatchBean) {
        try {
            if (gameBasketballMatchBean != null && !gameBasketballMatchBean.isEmpty() && gameBasketBallMatchBeanList != null && !gameBasketBallMatchBeanList.isEmpty() && getGroupCount() > 0) {
                AppLog.e("赛事列表收到通知：刷新篮球全部列表");
                for (int i = 0; i < gameBasketBallMatchBeanList.size(); i++) {
                    for (int j = 0; j < gameBasketBallMatchBeanList.get(i).getList().size(); j++) {
                        for (int k = 0; k < gameBasketballMatchBean.size(); k++) {
                            GameBasketballMatchBean gameBasketballMatch = (GameBasketballMatchBean) gameBasketballMatchBean.get(k);
                            if (gameBasketballMatch.getMatchId().equals(gameBasketBallMatchBeanList.get(i).getList().get(j).getMatchId())) {
                                AppLog.e("赛事列表收到通知：刷新篮球全部列表，匹配上ID：" + gameBasketballMatch.getMatchId());
                                gameBasketBallMatchBeanList.get(i).getList().get(j).setIs_playing(gameBasketballMatch.isIs_playing());
                                gameBasketBallMatchBeanList.get(i).getList().get(j).setHomeScore(gameBasketballMatch.getHomeScore());
                                gameBasketBallMatchBeanList.get(i).getList().get(j).setAwayScore(gameBasketballMatch.getAwayScore());
                            }
                        }
                    }
                }
                notifyDataChanged();
            }
        } catch (Exception e) {
            AppLog.e(e.toString());
        }
    }
}