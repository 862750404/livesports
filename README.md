## 接口服务列表：
   ### 首页
        √ 订阅和取消订阅主播(新接口)
        √ 关注推荐主播
        - 消费榜单
        - 获取分类下的直播
        - 配置信息
        - 获取过滤词汇
        - 获取关注主播列表
        - 获取热门主播
        - 登录方式开关信息
        - 获取附近主播
        - 获取最新主播
        - 推荐主播
        √ 获取推荐主播(新接口)
        √ 获取推荐直播
        √ 获取首页轮播图及赛事列表
        - 收益榜单
        - 搜索
        
   ### 赛事
        √ LOL赛事列表-全部
        √ LOL赛事列表查询
        √ LOL比赛分析查询
        √ LOL比赛数据查询
        √ LOL比赛指数查询
        √ LOL比赛详情查询
        - LOL比赛阵容查询
        √ LOL比赛选手查询
        
   ### 登陆注册


   ### 我的
       √ 订阅主播(新接口)


