package com.yunbao.main.utils.broccoli;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

public class Broccoli {
    protected final static int DEFAULT_PLACEHOLDER_COLOR = Color.parseColor("#dddddd");

    private BroccoliInternalImpl mPlaceholderInternalImpl;

    public Broccoli(){
        mPlaceholderInternalImpl = new BroccoliInternalImpl();
    }

    public Broccoli addPlaceholders(Activity activity, int ... viewIds){
        if (activity == null){
            return this;
        }
        addPlaceholders((ViewGroup) activity.findViewById(android.R.id.content), viewIds);
        return this;
    }

    public Broccoli addPlaceholders(ViewGroup parent, int ... viewIds){
        if (parent == null
                || viewIds == null){
            return this;
        }

        for (int id : viewIds){
            addPlaceholder(createDefaultParameter(parent.findViewById(id)));
        }

        return this;
    }

    public Broccoli addPlaceholders(View ... views){
        if (views == null){
            return this;
        }

        for (View view : views){
            addPlaceholder(createDefaultParameter(view));
        }

        return this;
    }

    private PlaceholderParameter createDefaultParameter(View view){
        return new PlaceholderParameter.Builder()
                .setView(view)
                .setColor(DEFAULT_PLACEHOLDER_COLOR)
                .build();
    }

    public Broccoli addPlaceholder(PlaceholderParameter parameter){
        if (parameter == null
                || parameter.getView() == null){
            return this;
        }
        mPlaceholderInternalImpl.addPlaceholder(parameter);
        return this;
    }

    public Broccoli addPlaceholder(List<PlaceholderParameter> placeholderParameters){
        if (placeholderParameters == null
                || placeholderParameters.isEmpty()){
            return this;
        }

        for (PlaceholderParameter parameter : placeholderParameters){
            addPlaceholder(parameter);
        }
        return this;
    }

    public Broccoli addPlaceholders(PlaceholderParameter... placeholderParameters){
        if (placeholderParameters == null
                || placeholderParameters.length == 0){
            return this;
        }

        for (PlaceholderParameter parameter : placeholderParameters){
            addPlaceholder(parameter);
        }
        return this;
    }

    public Broccoli removePlaceholder(View view){
        mPlaceholderInternalImpl.removePlaceholder(view);
        return this;
    }

    public Broccoli clearPlaceholder(View view){
        mPlaceholderInternalImpl.clearPlaceholder(view);
        return this;
    }

    public void removeAllPlaceholders(){
        mPlaceholderInternalImpl.hide(true);
    }

    public void clearAllPlaceholders(){
        mPlaceholderInternalImpl.hide(false);
    }

    public void show(){
        mPlaceholderInternalImpl.show();
    }
}
