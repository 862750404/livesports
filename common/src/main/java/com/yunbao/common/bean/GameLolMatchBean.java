package com.yunbao.common.bean;

import java.io.Serializable;

public class GameLolMatchBean implements Serializable {

    /**
     * id : 80
     * start_time : 2020-10-05
     * league_id : 634
     * match_id : 66813
     * status : 2
     * viewnum : 2013
     * team_a_score : 1
     * team_b_score : 0
     * round_son_name : 第三日
     * battle_list : [{"battle_id":6681301,"match_id":66813,"duration":1930,"index":1,"economic_diff":[{"time":0,"diff":0},{"time":60,"diff":0},{"time":120,"diff":14},{"time":180,"diff":-71},{"time":240,"diff":-105},{"time":300,"diff":-745},{"time":360,"diff":-1059},{"time":420,"diff":-1260},{"time":480,"diff":-746},{"time":540,"diff":-1369},{"time":600,"diff":-1122},{"time":660,"diff":-1683},{"time":720,"diff":-1764},{"time":780,"diff":-2976},{"time":840,"diff":-3049},{"time":900,"diff":-3010},{"time":960,"diff":-2529},{"time":1020,"diff":-2563},{"time":1080,"diff":-2267},{"time":1140,"diff":-1994},{"time":1200,"diff":-2015},{"time":1260,"diff":-2347},{"time":1320,"diff":-1281},{"time":1380,"diff":-1800},{"time":1440,"diff":-430},{"time":1500,"diff":867},{"time":1560,"diff":-2130},{"time":1620,"diff":-3956},{"time":1680,"diff":-3108},{"time":1740,"diff":-2881},{"time":1800,"diff":-2578},{"time":1860,"diff":-2193},{"time":1920,"diff":1448}],"xp_diff":[],"status":1,"start_time":0}]
     * startdate : 10-05
     * starttime : 21:00
     * league_name : S10 世界总决赛
     * battle_current_index : 1
     * battle_current_id : 6681301
     * battle_duration : 1930
     * team_a_name : TES
     * team_a_logo : https://qn.feijing88.com/egame/lol/team/fcbf7279a5638b6bc9605ab1a1991798.png
     * team_b_name : DRX
     * team_b_logo : https://qn.feijing88.com/egame/lol/team/3a8dff67d183e1c1d5ace4885a2ad922.png
     * league_cat : 英雄联盟
     * team_a_odds : 1.47
     * team_b_odds : 2.63
     * team_a_kill_count : 0
     * team_b_kill_count : 0
     */

    private String timeType;
    private String id;
    private String start_time;
    private String league_id;
    private String match_id;
    private int status;
    private String viewnum;
    private String team_a_score;
    private String team_b_score;
    private String round_son_name;
    private String battle_list;
    private String startdate;
    private String starttime;
    private String league_name;
    private int battle_current_index;
    private String battle_current_id;
    private String battle_duration;
    private String team_a_name;
    private String team_a_logo;
    private String team_b_name;
    private String team_b_logo;
    private String league_cat;
    private String team_a_odds;
    private String team_b_odds;
    private String team_a_kill_count;
    private String team_b_kill_count;
    private int match_type;//1-电竟 2-蓝球  3-足球

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getLeague_id() {
        return league_id;
    }

    public void setLeague_id(String league_id) {
        this.league_id = league_id;
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getViewnum() {
        return viewnum;
    }

    public void setViewnum(String viewnum) {
        this.viewnum = viewnum;
    }

    public String getTeam_a_score() {
        return team_a_score;
    }

    public void setTeam_a_score(String team_a_score) {
        this.team_a_score = team_a_score;
    }

    public String getTeam_b_score() {
        return team_b_score;
    }

    public void setTeam_b_score(String team_b_score) {
        this.team_b_score = team_b_score;
    }

    public String getRound_son_name() {
        return round_son_name;
    }

    public void setRound_son_name(String round_son_name) {
        this.round_son_name = round_son_name;
    }

    public String getBattle_list() {
        return battle_list;
    }

    public void setBattle_list(String battle_list) {
        this.battle_list = battle_list;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getLeague_name() {
        return league_name;
    }

    public void setLeague_name(String league_name) {
        this.league_name = league_name;
    }

    public int getBattle_current_index() {
        return battle_current_index;
    }

    public void setBattle_current_index(int battle_current_index) {
        this.battle_current_index = battle_current_index;
    }

    public String getBattle_current_id() {
        return battle_current_id;
    }

    public void setBattle_current_id(String battle_current_id) {
        this.battle_current_id = battle_current_id;
    }

    public String getBattle_duration() {
        return battle_duration;
    }

    public void setBattle_duration(String battle_duration) {
        this.battle_duration = battle_duration;
    }

    public String getTeam_a_name() {
        return team_a_name;
    }

    public void setTeam_a_name(String team_a_name) {
        this.team_a_name = team_a_name;
    }

    public String getTeam_a_logo() {
        return team_a_logo;
    }

    public void setTeam_a_logo(String team_a_logo) {
        this.team_a_logo = team_a_logo;
    }

    public String getTeam_b_name() {
        return team_b_name;
    }

    public void setTeam_b_name(String team_b_name) {
        this.team_b_name = team_b_name;
    }

    public String getTeam_b_logo() {
        return team_b_logo;
    }

    public void setTeam_b_logo(String team_b_logo) {
        this.team_b_logo = team_b_logo;
    }

    public String getLeague_cat() {
        return league_cat;
    }

    public void setLeague_cat(String league_cat) {
        this.league_cat = league_cat;
    }

    public String getTeam_a_odds() {
        return team_a_odds;
    }

    public void setTeam_a_odds(String team_a_odds) {
        this.team_a_odds = team_a_odds;
    }

    public String getTeam_b_odds() {
        return team_b_odds;
    }

    public void setTeam_b_odds(String team_b_odds) {
        this.team_b_odds = team_b_odds;
    }

    public String getTeam_a_kill_count() {
        return team_a_kill_count;
    }

    public void setTeam_a_kill_count(String team_a_kill_count) {
        this.team_a_kill_count = team_a_kill_count;
    }

    public String getTeam_b_kill_count() {
        return team_b_kill_count;
    }

    public void setTeam_b_kill_count(String team_b_kill_count) {
        this.team_b_kill_count = team_b_kill_count;
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public int getMatch_type() {
        return match_type;
    }

    public void setMatch_type(int match_type) {
        this.match_type = match_type;
    }

    @Override
    public String toString() {
        return "GameLolMatchBean{" +
                "timeType='" + timeType + '\'' +
                ", id='" + id + '\'' +
                ", start_time='" + start_time + '\'' +
                ", league_id='" + league_id + '\'' +
                ", match_id='" + match_id + '\'' +
                ", status=" + status +
                ", viewnum='" + viewnum + '\'' +
                ", team_a_score='" + team_a_score + '\'' +
                ", team_b_score='" + team_b_score + '\'' +
                ", round_son_name='" + round_son_name + '\'' +
                ", battle_list='" + battle_list + '\'' +
                ", startdate='" + startdate + '\'' +
                ", starttime='" + starttime + '\'' +
                ", league_name='" + league_name + '\'' +
                ", battle_current_index=" + battle_current_index +
                ", battle_current_id='" + battle_current_id + '\'' +
                ", battle_duration='" + battle_duration + '\'' +
                ", team_a_name='" + team_a_name + '\'' +
                ", team_a_logo='" + team_a_logo + '\'' +
                ", team_b_name='" + team_b_name + '\'' +
                ", team_b_logo='" + team_b_logo + '\'' +
                ", league_cat='" + league_cat + '\'' +
                ", team_a_odds='" + team_a_odds + '\'' +
                ", team_b_odds='" + team_b_odds + '\'' +
                ", team_a_kill_count='" + team_a_kill_count + '\'' +
                ", team_b_kill_count='" + team_b_kill_count + '\'' +
                ", match_type=" + match_type +
                '}';
    }
}
