package com.yunbao.main.adapter.home;

import android.annotation.SuppressLint;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yunbao.common.bean.GameLolMatchBean;
import com.yunbao.main.R;

import java.util.List;

/**
 * 首页游戏
 */
public class HomeGameAdapter extends BaseQuickAdapter<GameLolMatchBean, BaseViewHolder> {

    public HomeGameAdapter(@LayoutRes int layoutResId, @Nullable List<GameLolMatchBean> data) {
        super(layoutResId, data);
    }

    RequestOptions options = new RequestOptions()
            .placeholder(R.mipmap.icon_default_logo)
            .error(R.mipmap.icon_default_logo);

    @SuppressLint("ResourceAsColor")
    @Override
    protected void convert(BaseViewHolder helper, GameLolMatchBean item) {

        TextView tv_item_home_game_title = helper.getView(R.id.tv_item_home_game_title);
        TextView tv_game_status = helper.getView(R.id.tv_game_status);
        TextView tv_game_result = helper.getView(R.id.tv_game_result);
        ImageView iv_left = helper.getView(R.id.iv_left);
        ImageView iv_right = helper.getView(R.id.iv_right);
        TextView tv_item_home_game_hint = helper.getView(R.id.tv_item_home_game_hint);
        TextView tv_team_a_name = helper.getView(R.id.tv_team_a_name);
        TextView tv_team_b_name = helper.getView(R.id.tv_team_b_name);

        //游戏分类
        tv_item_home_game_title.setText(item.getLeague_cat());
        tv_team_a_name.setText(item.getTeam_a_name());
        tv_team_b_name.setText(item.getTeam_b_name());
//        String dateDes = DateFormatUtil.getDateDes(item.getStarttime());
        //联赛名称
        tv_item_home_game_hint.setText(item.getLeague_name());
        //比赛状态 0:未开始 1:进行中 2:已结束 3:已延期 4:已删除
        if (item.getStatus() == 1) {
            tv_game_status.setText("比赛中");
            tv_game_status.setBackgroundResource(R.drawable.bg_home_game_status_in_play);
            tv_game_result.setText(String.format("%s : %s", item.getTeam_a_score(), item.getTeam_b_score()));
        } else if (item.getStatus() == 2) {
            tv_game_status.setText("完");
            tv_game_status.setBackgroundResource(R.drawable.bg_home_game_status_not_play);
            tv_game_result.setText(String.format("%s %s", item.getTeam_a_score(), item.getTeam_b_score()));
        } else {
            tv_game_status.setText("未开始");
            tv_game_status.setBackgroundResource(R.drawable.bg_home_game_status_not_play);
            tv_game_result.setText(item.getStarttime());
        }

        Glide.with(mContext)
                .load(item.getTeam_a_logo())
                .apply(options)
                .into(iv_left);

        Glide.with(mContext)
                .load(item.getTeam_b_logo())
                .apply(options)
                .into(iv_right);

    }
}
