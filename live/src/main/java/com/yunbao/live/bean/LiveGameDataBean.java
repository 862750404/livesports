package com.yunbao.live.bean;

import java.util.List;

public class LiveGameDataBean {


    /**
     * id : 45
     * start_time : 2020-09-14
     * league_id : 268176421
     * match_id : 268176573
     * status : 2
     * viewnum : 2013
     * team_a_score : 0
     * team_b_score : 1
     * team_a_id : 2680672363
     * team_b_id : 2680234999
     * round_son_name :
     * startdate : 09-14
     * starttime : 01:00
     * league_name : 2020欧洲大师赛夏季赛
     * battle_current_index : 1
     * battle_current_id : 6688401
     * battle_duration : 0
     * team_a_name : AGO
     * team_a_logo : https://qn.feijing88.com/feijing-home/egame/image/2020916/3bab07c6b04b42498fcbf7e3391eed74.png
     * team_b_name :
     * team_b_logo : https://qn.feijing88.com/egame/lol/team/458704417899353d49db4ce94f22ac82.png
     * league_cat : 英雄联盟
     * data : {"duration":"28:58","index":"1","economic_diff":[{"time":0,"diff":0},{"time":60,"diff":0},{"time":120,"diff":100},{"time":180,"diff":300},{"time":240,"diff":0},{"time":300,"diff":0},{"time":360,"diff":100},{"time":420,"diff":200},{"time":480,"diff":100},{"time":540,"diff":-100},{"time":600,"diff":-300},{"time":660,"diff":-700},{"time":720,"diff":-700},{"time":780,"diff":-100},{"time":840,"diff":1000},{"time":900,"diff":1100},{"time":960,"diff":1200},{"time":1020,"diff":1100},{"time":1080,"diff":1200},{"time":1140,"diff":1300},{"time":1200,"diff":1900},{"time":1260,"diff":2700},{"time":1320,"diff":4400},{"time":1380,"diff":6400},{"time":1440,"diff":9000},{"time":1500,"diff":9500},{"time":1560,"diff":9800},{"time":1620,"diff":8900},{"time":1680,"diff":9800}],"xp_diff":[],"creep_score_diff":null,"player_stats":[{"player_name":"Armut","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","team_id":2680672363,"hero_name":"齐天大圣","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/0a6ae71a384c44e98815c0fb40135579.png","kill_count":7,"death_count":0,"assist_count":8,"last_hit_count":228,"kda":15,"money_count":0,"equip_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/33e3a8304c6f44259c0aeb200d048fb7.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/b08a2d6115234eff98f04461592a30c3.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/e1cc9e86a6f04f8ba6144a744037efd4.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/3eeb32575d4e49d38af5fe3615bbf630.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"KaKAO","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","team_id":2680672363,"hero_name":"狂野女猎手","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e90ed3f198064e9190426cf95986b02d.png","kill_count":1,"death_count":0,"assist_count":15,"last_hit_count":200,"kda":16,"money_count":0,"equip_ids":["https://qn.feijing88.com/egame/lol/item/0bf81c654c1a010f87dacf13772500d0.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/1e38d7ac6e7b490e80d5be58ede621a7.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/2fcd3a9a820f4850ab8bb40a0e267db7.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/32ad7c1c909e4153aaffd2af69b164f0.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"Bolulu","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","team_id":2680672363,"hero_name":"沙漠皇帝","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png","kill_count":4,"death_count":3,"assist_count":9,"last_hit_count":251,"kda":4.3,"money_count":0,"equip_ids":["https://qn.feijing88.com/feijing-home/egame/image/20181120/e1cc9e86a6f04f8ba6144a744037efd4.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/9bbf946fe4d44f0faac42bcdbbb627b9.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/9bbf946fe4d44f0faac42bcdbbb627b9.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/3be665cd560d4ab682e3277858a1908a.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/0f4f99bed3d2446bbf5d9241390f6a33.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/5084892d5d22457a84ada97210854c2f.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"Zeitnot","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","team_id":2680672363,"hero_name":"涤魂圣枪","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20191228/69428d235950432d8853b63e6f739370.png","kill_count":4,"death_count":1,"assist_count":10,"last_hit_count":165,"kda":14,"money_count":0,"equip_ids":["https://qn.feijing88.com/egame/lol/item/0e0adf3844d3d2610cb5a9bd9cb79e9f.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/bd2245bd97f240b2a5b3a83040b28518.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/b43ebfa117e0474bafc7730f9312d8c1.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/5084892d5d22457a84ada97210854c2f.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png","https://qn.feijing88.com/egame/lol/skill/9cb5b87fbf4c844993e168a027730697.png"]},{"player_name":"SnowFlower","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/d758accd60c349fdb0d501684a701d0d.png","team_id":2680672363,"hero_name":"曙光女神","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a6b45077c0a645888336e4d0f308d6ec.png","kill_count":2,"death_count":1,"assist_count":9,"last_hit_count":34,"kda":11,"money_count":0,"equip_ids":["https://qn.feijing88.com/feijing-home/egame/image/20181120/52eab0221da542c2a49b12cc9893377d.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/e1cc9e86a6f04f8ba6144a744037efd4.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/839631f8f4914911a3099b5fa9f1a32a.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"Orome","player_avatar":"https://qn.feijing88.com/egame/lol/player/6ef39a2d3dcd598c92e44420fceafc31.png","team_id":2680234999,"hero_name":"铁铠冥魂","hero_avatar":"https://qn.feijing88.com/egame/lol/hero/b2ae48389d6395d708fdd200f9d133fb.png","kill_count":1,"death_count":4,"assist_count":1,"last_hit_count":245,"kda":0.5,"money_count":0,"equip_ids":["https://qn.feijing88.com/feijing-home/egame/image/20181120/1e38d7ac6e7b490e80d5be58ede621a7.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/b3fd82b4919d40389a1994b598f7ce40.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/a37880108a9e4db8bde31db6d803c281.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/0d8a20fd133d42e180f17c8a3ecac77e.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/0f4f99bed3d2446bbf5d9241390f6a33.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/df61c73cc60c41c89116e814199e1daa.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"Shadow","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/2020923/766672f1e81c4f919d096c47f027ca52.png","team_id":2680234999,"hero_name":"战争之影","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/7f210483106d4dd0976284c233b92b10.jpg","kill_count":2,"death_count":5,"assist_count":2,"last_hit_count":188,"kda":0.8,"money_count":0,"equip_ids":["https://qn.feijing88.com/egame/lol/item/0bf81c654c1a010f87dacf13772500d0.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/c925710f9876403d9cd5ef361a37a35c.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/9e1f8b5671444036b597e896cffcd200.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/e1cc9e86a6f04f8ba6144a744037efd4.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/051586eedf5b453ba7b6c7be291252ef.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/3eeb32575d4e49d38af5fe3615bbf630.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"],"skill_ids":[]},{"player_name":"Humanoid","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/2020923/d376fb1dbf2847adb728bc4f57619641.png","team_id":2680234999,"hero_name":"暮光星灵","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a77a4745ff474125b2d3b2d262792000.png","kill_count":0,"death_count":5,"assist_count":3,"last_hit_count":210,"kda":0.6,"money_count":0,"equip_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/6f7d2759343d467cb8d76123907eff1c.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/b0c0e6c5b4754d3b934f9ce65f98bd42.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/48fffb5085084f068d918c050e436030.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/197d3e4a07a2419dbc379d022d084a5f.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/5084892d5d22457a84ada97210854c2f.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png","https://qn.feijing88.com/egame/lol/skill/9cb5b87fbf4c844993e168a027730697.png"]},{"player_name":"Carzzy","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/2020923/a707476e7dfc43c380b534af6f5b13ad.png","team_id":2680234999,"hero_name":"瘟疫之源","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/5f11380347214e54a20a5b23966f0777.png","kill_count":2,"death_count":3,"assist_count":1,"last_hit_count":250,"kda":1,"money_count":0,"equip_ids":["https://qn.feijing88.com/egame/lol/item/0bf81c654c1a010f87dacf13772500d0.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/1253f0f2b63049e7b9c9d60fdee9de07.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/6fe1b1f00aa94a528050c61e4250f73f.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/5084892d5d22457a84ada97210854c2f.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"Kaiser","player_avatar":"https://qn.feijing88.com/egame/lol/player/e58fa82e5a507330060e0ab09eeda3a2.png","team_id":2680234999,"hero_name":"幻翎","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/09f540667e724336b60af2a640ea1267.jpg","kill_count":0,"death_count":1,"assist_count":3,"last_hit_count":35,"kda":3,"money_count":0,"equip_ids":["https://qn.feijing88.com/egame/lol/item/0bf81c654c1a010f87dacf13772500d0.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/cc6a8bde51cb4ab98d3a5f104a99cdc3.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/e433df51d414470e90535d29d435281d.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/2fcd3a9a820f4850ab8bb40a0e267db7.jpg","https://qn.feijing88.com/egame/lol/item/717f42281d26651db0b360f54948ddcf.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png","https://qn.feijing88.com/egame/lol/skill/9cb5b87fbf4c844993e168a027730697.png"]}],"team_stats":[{"team_id":2680672363,"team_name":"","team_avatar":"https://qn.feijing88.com/egame/lol/team/c843b3139168cb799d2251d528e872b8.png","big_dragon_count":1,"small_dragon_count":2,"tower_success_count":10,"inhibitor_success_count":0,"is_win":false,"side":"blue","pick_list":[{"hero_id":10,"name":"沙漠皇帝","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png"},{"hero_id":59,"name":"曙光女神","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a6b45077c0a645888336e4d0f308d6ec.png"},{"hero_id":69,"name":"齐天大圣","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/0a6ae71a384c44e98815c0fb40135579.png"},{"hero_id":75,"name":"狂野女猎手","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e90ed3f198064e9190426cf95986b02d.png"},{"hero_id":26807148,"name":"涤魂圣枪","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20191228/69428d235950432d8853b63e6f739370.png"}],"ban_list":[{"hero_id":10,"name":"沙漠皇帝","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png"},{"hero_id":59,"name":"曙光女神","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a6b45077c0a645888336e4d0f308d6ec.png"},{"hero_id":69,"name":"齐天大圣","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/0a6ae71a384c44e98815c0fb40135579.png"},{"hero_id":75,"name":"狂野女猎手","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e90ed3f198064e9190426cf95986b02d.png"},{"hero_id":26807148,"name":"涤魂圣枪","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20191228/69428d235950432d8853b63e6f739370.png"}]},{"team_id":2680234999,"team_name":"","team_avatar":"https://qn.feijing88.com/egame/lol/team/444e408cac279c5ffb3a8336131aa607.png","big_dragon_count":0,"small_dragon_count":1,"tower_success_count":2,"inhibitor_success_count":0,"is_win":false,"side":"red","pick_list":[{"hero_id":36,"name":"战争之影","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/7f210483106d4dd0976284c233b92b10.jpg"},{"hero_id":70,"name":"铁铠冥魂","avatar":"https://qn.feijing88.com/egame/lol/hero/b2ae48389d6395d708fdd200f9d133fb.png"},{"hero_id":112,"name":"瘟疫之源","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/5f11380347214e54a20a5b23966f0777.png"},{"hero_id":201,"name":"幻翎","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/09f540667e724336b60af2a640ea1267.jpg"},{"hero_id":211,"name":"暮光星灵","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a77a4745ff474125b2d3b2d262792000.png"}],"ban_list":[{"hero_id":36,"name":"战争之影","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/7f210483106d4dd0976284c233b92b10.jpg"},{"hero_id":70,"name":"铁铠冥魂","avatar":"https://qn.feijing88.com/egame/lol/hero/b2ae48389d6395d708fdd200f9d133fb.png"},{"hero_id":112,"name":"瘟疫之源","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/5f11380347214e54a20a5b23966f0777.png"},{"hero_id":201,"name":"幻翎","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/09f540667e724336b60af2a640ea1267.jpg"},{"hero_id":211,"name":"暮光星灵","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a77a4745ff474125b2d3b2d262792000.png"}]}]}
     */

    private String id;
    private String start_time;
    private String league_id;
    private String match_id;
    private String status;
    private int viewnum;
    private String team_a_score;
    private String team_b_score;
    private String team_a_id;
    private String team_b_id;
    private String round_son_name;
    private String startdate;
    private String starttime;
    private String league_name;
    private int battle_current_index;
    private int battle_current_id;
    private int battle_duration;
    private String team_a_name;
    private String team_a_logo;
    private String team_b_name;
    private String team_b_logo;
    private String league_cat;
    private DataBean data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getLeague_id() {
        return league_id;
    }

    public void setLeague_id(String league_id) {
        this.league_id = league_id;
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getViewnum() {
        return viewnum;
    }

    public void setViewnum(int viewnum) {
        this.viewnum = viewnum;
    }

    public String getTeam_a_score() {
        return team_a_score;
    }

    public void setTeam_a_score(String team_a_score) {
        this.team_a_score = team_a_score;
    }

    public String getTeam_b_score() {
        return team_b_score;
    }

    public void setTeam_b_score(String team_b_score) {
        this.team_b_score = team_b_score;
    }

    public String getTeam_a_id() {
        return team_a_id;
    }

    public void setTeam_a_id(String team_a_id) {
        this.team_a_id = team_a_id;
    }

    public String getTeam_b_id() {
        return team_b_id;
    }

    public void setTeam_b_id(String team_b_id) {
        this.team_b_id = team_b_id;
    }

    public String getRound_son_name() {
        return round_son_name;
    }

    public void setRound_son_name(String round_son_name) {
        this.round_son_name = round_son_name;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getLeague_name() {
        return league_name;
    }

    public void setLeague_name(String league_name) {
        this.league_name = league_name;
    }

    public int getBattle_current_index() {
        return battle_current_index;
    }

    public void setBattle_current_index(int battle_current_index) {
        this.battle_current_index = battle_current_index;
    }

    public int getBattle_current_id() {
        return battle_current_id;
    }

    public void setBattle_current_id(int battle_current_id) {
        this.battle_current_id = battle_current_id;
    }

    public int getBattle_duration() {
        return battle_duration;
    }

    public void setBattle_duration(int battle_duration) {
        this.battle_duration = battle_duration;
    }

    public String getTeam_a_name() {
        return team_a_name;
    }

    public void setTeam_a_name(String team_a_name) {
        this.team_a_name = team_a_name;
    }

    public String getTeam_a_logo() {
        return team_a_logo;
    }

    public void setTeam_a_logo(String team_a_logo) {
        this.team_a_logo = team_a_logo;
    }

    public String getTeam_b_name() {
        return team_b_name;
    }

    public void setTeam_b_name(String team_b_name) {
        this.team_b_name = team_b_name;
    }

    public String getTeam_b_logo() {
        return team_b_logo;
    }

    public void setTeam_b_logo(String team_b_logo) {
        this.team_b_logo = team_b_logo;
    }

    public String getLeague_cat() {
        return league_cat;
    }

    public void setLeague_cat(String league_cat) {
        this.league_cat = league_cat;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * duration : 28:58
         * index : 1
         * economic_diff : [{"time":0,"diff":0},{"time":60,"diff":0},{"time":120,"diff":100},{"time":180,"diff":300},{"time":240,"diff":0},{"time":300,"diff":0},{"time":360,"diff":100},{"time":420,"diff":200},{"time":480,"diff":100},{"time":540,"diff":-100},{"time":600,"diff":-300},{"time":660,"diff":-700},{"time":720,"diff":-700},{"time":780,"diff":-100},{"time":840,"diff":1000},{"time":900,"diff":1100},{"time":960,"diff":1200},{"time":1020,"diff":1100},{"time":1080,"diff":1200},{"time":1140,"diff":1300},{"time":1200,"diff":1900},{"time":1260,"diff":2700},{"time":1320,"diff":4400},{"time":1380,"diff":6400},{"time":1440,"diff":9000},{"time":1500,"diff":9500},{"time":1560,"diff":9800},{"time":1620,"diff":8900},{"time":1680,"diff":9800}]
         * xp_diff : []
         * creep_score_diff : null
         * player_stats : [{"player_name":"Armut","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","team_id":2680672363,"hero_name":"齐天大圣","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/0a6ae71a384c44e98815c0fb40135579.png","kill_count":7,"death_count":0,"assist_count":8,"last_hit_count":228,"kda":15,"money_count":0,"equip_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/33e3a8304c6f44259c0aeb200d048fb7.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/b08a2d6115234eff98f04461592a30c3.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/e1cc9e86a6f04f8ba6144a744037efd4.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/3eeb32575d4e49d38af5fe3615bbf630.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"KaKAO","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","team_id":2680672363,"hero_name":"狂野女猎手","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e90ed3f198064e9190426cf95986b02d.png","kill_count":1,"death_count":0,"assist_count":15,"last_hit_count":200,"kda":16,"money_count":0,"equip_ids":["https://qn.feijing88.com/egame/lol/item/0bf81c654c1a010f87dacf13772500d0.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/1e38d7ac6e7b490e80d5be58ede621a7.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/2fcd3a9a820f4850ab8bb40a0e267db7.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/32ad7c1c909e4153aaffd2af69b164f0.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"Bolulu","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","team_id":2680672363,"hero_name":"沙漠皇帝","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png","kill_count":4,"death_count":3,"assist_count":9,"last_hit_count":251,"kda":4.3,"money_count":0,"equip_ids":["https://qn.feijing88.com/feijing-home/egame/image/20181120/e1cc9e86a6f04f8ba6144a744037efd4.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/9bbf946fe4d44f0faac42bcdbbb627b9.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/9bbf946fe4d44f0faac42bcdbbb627b9.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/3be665cd560d4ab682e3277858a1908a.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/0f4f99bed3d2446bbf5d9241390f6a33.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/5084892d5d22457a84ada97210854c2f.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"Zeitnot","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","team_id":2680672363,"hero_name":"涤魂圣枪","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20191228/69428d235950432d8853b63e6f739370.png","kill_count":4,"death_count":1,"assist_count":10,"last_hit_count":165,"kda":14,"money_count":0,"equip_ids":["https://qn.feijing88.com/egame/lol/item/0e0adf3844d3d2610cb5a9bd9cb79e9f.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/bd2245bd97f240b2a5b3a83040b28518.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/b43ebfa117e0474bafc7730f9312d8c1.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/5084892d5d22457a84ada97210854c2f.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png","https://qn.feijing88.com/egame/lol/skill/9cb5b87fbf4c844993e168a027730697.png"]},{"player_name":"SnowFlower","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/d758accd60c349fdb0d501684a701d0d.png","team_id":2680672363,"hero_name":"曙光女神","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a6b45077c0a645888336e4d0f308d6ec.png","kill_count":2,"death_count":1,"assist_count":9,"last_hit_count":34,"kda":11,"money_count":0,"equip_ids":["https://qn.feijing88.com/feijing-home/egame/image/20181120/52eab0221da542c2a49b12cc9893377d.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/e1cc9e86a6f04f8ba6144a744037efd4.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/839631f8f4914911a3099b5fa9f1a32a.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"Orome","player_avatar":"https://qn.feijing88.com/egame/lol/player/6ef39a2d3dcd598c92e44420fceafc31.png","team_id":2680234999,"hero_name":"铁铠冥魂","hero_avatar":"https://qn.feijing88.com/egame/lol/hero/b2ae48389d6395d708fdd200f9d133fb.png","kill_count":1,"death_count":4,"assist_count":1,"last_hit_count":245,"kda":0.5,"money_count":0,"equip_ids":["https://qn.feijing88.com/feijing-home/egame/image/20181120/1e38d7ac6e7b490e80d5be58ede621a7.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/b3fd82b4919d40389a1994b598f7ce40.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/a37880108a9e4db8bde31db6d803c281.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/0d8a20fd133d42e180f17c8a3ecac77e.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/0f4f99bed3d2446bbf5d9241390f6a33.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/df61c73cc60c41c89116e814199e1daa.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"Shadow","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/2020923/766672f1e81c4f919d096c47f027ca52.png","team_id":2680234999,"hero_name":"战争之影","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/7f210483106d4dd0976284c233b92b10.jpg","kill_count":2,"death_count":5,"assist_count":2,"last_hit_count":188,"kda":0.8,"money_count":0,"equip_ids":["https://qn.feijing88.com/egame/lol/item/0bf81c654c1a010f87dacf13772500d0.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/c925710f9876403d9cd5ef361a37a35c.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/9e1f8b5671444036b597e896cffcd200.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/e1cc9e86a6f04f8ba6144a744037efd4.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/051586eedf5b453ba7b6c7be291252ef.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/3eeb32575d4e49d38af5fe3615bbf630.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"],"skill_ids":[]},{"player_name":"Humanoid","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/2020923/d376fb1dbf2847adb728bc4f57619641.png","team_id":2680234999,"hero_name":"暮光星灵","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a77a4745ff474125b2d3b2d262792000.png","kill_count":0,"death_count":5,"assist_count":3,"last_hit_count":210,"kda":0.6,"money_count":0,"equip_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/6f7d2759343d467cb8d76123907eff1c.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/b0c0e6c5b4754d3b934f9ce65f98bd42.jpg","https://qn.feijing88.com/feijing-home/egame/image/20190629/48fffb5085084f068d918c050e436030.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/197d3e4a07a2419dbc379d022d084a5f.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/5084892d5d22457a84ada97210854c2f.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png","https://qn.feijing88.com/egame/lol/skill/9cb5b87fbf4c844993e168a027730697.png"]},{"player_name":"Carzzy","player_avatar":"https://qn.feijing88.com/feijing-home/egame/image/2020923/a707476e7dfc43c380b534af6f5b13ad.png","team_id":2680234999,"hero_name":"瘟疫之源","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/5f11380347214e54a20a5b23966f0777.png","kill_count":2,"death_count":3,"assist_count":1,"last_hit_count":250,"kda":1,"money_count":0,"equip_ids":["https://qn.feijing88.com/egame/lol/item/0bf81c654c1a010f87dacf13772500d0.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/1253f0f2b63049e7b9c9d60fdee9de07.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/6fe1b1f00aa94a528050c61e4250f73f.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/5084892d5d22457a84ada97210854c2f.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]},{"player_name":"Kaiser","player_avatar":"https://qn.feijing88.com/egame/lol/player/e58fa82e5a507330060e0ab09eeda3a2.png","team_id":2680234999,"hero_name":"幻翎","hero_avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/09f540667e724336b60af2a640ea1267.jpg","kill_count":0,"death_count":1,"assist_count":3,"last_hit_count":35,"kda":3,"money_count":0,"equip_ids":["https://qn.feijing88.com/egame/lol/item/0bf81c654c1a010f87dacf13772500d0.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/cc6a8bde51cb4ab98d3a5f104a99cdc3.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/e433df51d414470e90535d29d435281d.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/2fcd3a9a820f4850ab8bb40a0e267db7.jpg","https://qn.feijing88.com/egame/lol/item/717f42281d26651db0b360f54948ddcf.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"],"skill_ids":["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png","https://qn.feijing88.com/egame/lol/skill/9cb5b87fbf4c844993e168a027730697.png"]}]
         * team_stats : [{"team_id":2680672363,"team_name":"","team_avatar":"https://qn.feijing88.com/egame/lol/team/c843b3139168cb799d2251d528e872b8.png","big_dragon_count":1,"small_dragon_count":2,"tower_success_count":10,"inhibitor_success_count":0,"is_win":false,"side":"blue","pick_list":[{"hero_id":10,"name":"沙漠皇帝","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png"},{"hero_id":59,"name":"曙光女神","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a6b45077c0a645888336e4d0f308d6ec.png"},{"hero_id":69,"name":"齐天大圣","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/0a6ae71a384c44e98815c0fb40135579.png"},{"hero_id":75,"name":"狂野女猎手","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e90ed3f198064e9190426cf95986b02d.png"},{"hero_id":26807148,"name":"涤魂圣枪","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20191228/69428d235950432d8853b63e6f739370.png"}],"ban_list":[{"hero_id":10,"name":"沙漠皇帝","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png"},{"hero_id":59,"name":"曙光女神","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a6b45077c0a645888336e4d0f308d6ec.png"},{"hero_id":69,"name":"齐天大圣","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/0a6ae71a384c44e98815c0fb40135579.png"},{"hero_id":75,"name":"狂野女猎手","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e90ed3f198064e9190426cf95986b02d.png"},{"hero_id":26807148,"name":"涤魂圣枪","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20191228/69428d235950432d8853b63e6f739370.png"}]},{"team_id":2680234999,"team_name":"","team_avatar":"https://qn.feijing88.com/egame/lol/team/444e408cac279c5ffb3a8336131aa607.png","big_dragon_count":0,"small_dragon_count":1,"tower_success_count":2,"inhibitor_success_count":0,"is_win":false,"side":"red","pick_list":[{"hero_id":36,"name":"战争之影","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/7f210483106d4dd0976284c233b92b10.jpg"},{"hero_id":70,"name":"铁铠冥魂","avatar":"https://qn.feijing88.com/egame/lol/hero/b2ae48389d6395d708fdd200f9d133fb.png"},{"hero_id":112,"name":"瘟疫之源","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/5f11380347214e54a20a5b23966f0777.png"},{"hero_id":201,"name":"幻翎","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/09f540667e724336b60af2a640ea1267.jpg"},{"hero_id":211,"name":"暮光星灵","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a77a4745ff474125b2d3b2d262792000.png"}],"ban_list":[{"hero_id":36,"name":"战争之影","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/7f210483106d4dd0976284c233b92b10.jpg"},{"hero_id":70,"name":"铁铠冥魂","avatar":"https://qn.feijing88.com/egame/lol/hero/b2ae48389d6395d708fdd200f9d133fb.png"},{"hero_id":112,"name":"瘟疫之源","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/5f11380347214e54a20a5b23966f0777.png"},{"hero_id":201,"name":"幻翎","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20181120/09f540667e724336b60af2a640ea1267.jpg"},{"hero_id":211,"name":"暮光星灵","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a77a4745ff474125b2d3b2d262792000.png"}]}]
         */

        private String duration;
        private String index;
        private Object creep_score_diff;
        private List<EconomicDiffBean> economic_diff;
        private List<?> xp_diff;
        private List<PlayerStatsBean> player_stats;
        private List<TeamStatsBean> team_stats;

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public Object getCreep_score_diff() {
            return creep_score_diff;
        }

        public void setCreep_score_diff(Object creep_score_diff) {
            this.creep_score_diff = creep_score_diff;
        }

        public List<EconomicDiffBean> getEconomic_diff() {
            return economic_diff;
        }

        public void setEconomic_diff(List<EconomicDiffBean> economic_diff) {
            this.economic_diff = economic_diff;
        }

        public List<?> getXp_diff() {
            return xp_diff;
        }

        public void setXp_diff(List<?> xp_diff) {
            this.xp_diff = xp_diff;
        }

        public List<PlayerStatsBean> getPlayer_stats() {
            return player_stats;
        }

        public void setPlayer_stats(List<PlayerStatsBean> player_stats) {
            this.player_stats = player_stats;
        }

        public List<TeamStatsBean> getTeam_stats() {
            return team_stats;
        }

        public void setTeam_stats(List<TeamStatsBean> team_stats) {
            this.team_stats = team_stats;
        }

        public static class EconomicDiffBean {
            /**
             * time : 0
             * diff : 0
             */

            private String time;
            private float diff;

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public float getDiff() {
                return diff;
            }

            public void setDiff(float diff) {
                this.diff = diff;
            }
        }

        public static class PlayerStatsBean {
            /**
             * player_name : Armut
             * player_avatar : https://qn.feijing88.com/feijing-home/egame/image/lol/player.png
             * team_id : 2680672363
             * hero_name : 齐天大圣
             * hero_avatar : https://qn.feijing88.com/feijing-home/egame/image/20190622/0a6ae71a384c44e98815c0fb40135579.png
             * kill_count : 7
             * death_count : 0
             * assist_count : 8
             * last_hit_count : 228
             * kda : 15
             * money_count : 0
             * equip_ids : ["https://qn.feijing88.com/feijing-home/egame/image/20190629/33e3a8304c6f44259c0aeb200d048fb7.png","https://qn.feijing88.com/feijing-home/egame/image/20190629/b08a2d6115234eff98f04461592a30c3.png","https://qn.feijing88.com/feijing-home/egame/image/20181120/e1cc9e86a6f04f8ba6144a744037efd4.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/3eeb32575d4e49d38af5fe3615bbf630.jpg","https://qn.feijing88.com/feijing-home/egame/image/20181120/a491cfff7e0b4c70b8d15d404a686fb8.jpg"]
             * skill_ids : ["https://qn.feijing88.com/feijing-home/egame/image/20190629/3568a02cc8854a78af1cf1ca84d66082.png"]
             */

            private String player_name;
            private String player_avatar;
            private String team_id;
            private String hero_name;
            private String hero_avatar;
            private String kill_count;
            private String death_count;
            private String assist_count;
            private String last_hit_count;
            private String kda;
            private String money_count;
            private List<String> equip_ids;
            private List<String> skill_ids;

            public String getPlayer_name() {
                return player_name;
            }

            public void setPlayer_name(String player_name) {
                this.player_name = player_name;
            }

            public String getPlayer_avatar() {
                return player_avatar;
            }

            public void setPlayer_avatar(String player_avatar) {
                this.player_avatar = player_avatar;
            }

            public String getTeam_id() {
                return team_id;
            }

            public void setTeam_id(String team_id) {
                this.team_id = team_id;
            }

            public String getHero_name() {
                return hero_name;
            }

            public void setHero_name(String hero_name) {
                this.hero_name = hero_name;
            }

            public String getHero_avatar() {
                return hero_avatar;
            }

            public void setHero_avatar(String hero_avatar) {
                this.hero_avatar = hero_avatar;
            }

            public String getKill_count() {
                return kill_count;
            }

            public void setKill_count(String kill_count) {
                this.kill_count = kill_count;
            }

            public String getDeath_count() {
                return death_count;
            }

            public void setDeath_count(String death_count) {
                this.death_count = death_count;
            }

            public String getAssist_count() {
                return assist_count;
            }

            public void setAssist_count(String assist_count) {
                this.assist_count = assist_count;
            }

            public String getLast_hit_count() {
                return last_hit_count;
            }

            public void setLast_hit_count(String last_hit_count) {
                this.last_hit_count = last_hit_count;
            }

            public String getKda() {
                return kda;
            }

            public void setKda(String kda) {
                this.kda = kda;
            }

            public String getMoney_count() {
                return money_count;
            }

            public void setMoney_count(String money_count) {
                this.money_count = money_count;
            }

            public List<String> getEquip_ids() {
                return equip_ids;
            }

            public void setEquip_ids(List<String> equip_ids) {
                this.equip_ids = equip_ids;
            }

            public List<String> getSkill_ids() {
                return skill_ids;
            }

            public void setSkill_ids(List<String> skill_ids) {
                this.skill_ids = skill_ids;
            }
        }

        public static class TeamStatsBean {
            /**
             * team_id : 2680672363
             * team_name :
             * team_avatar : https://qn.feijing88.com/egame/lol/team/c843b3139168cb799d2251d528e872b8.png
             * big_dragon_count : 1
             * small_dragon_count : 2
             * tower_success_count : 10
             * inhibitor_success_count : 0
             * is_win : false
             * side : blue
             * pick_list : [{"hero_id":10,"name":"沙漠皇帝","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png"},{"hero_id":59,"name":"曙光女神","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a6b45077c0a645888336e4d0f308d6ec.png"},{"hero_id":69,"name":"齐天大圣","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/0a6ae71a384c44e98815c0fb40135579.png"},{"hero_id":75,"name":"狂野女猎手","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e90ed3f198064e9190426cf95986b02d.png"},{"hero_id":26807148,"name":"涤魂圣枪","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20191228/69428d235950432d8853b63e6f739370.png"}]
             * ban_list : [{"hero_id":10,"name":"沙漠皇帝","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png"},{"hero_id":59,"name":"曙光女神","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a6b45077c0a645888336e4d0f308d6ec.png"},{"hero_id":69,"name":"齐天大圣","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/0a6ae71a384c44e98815c0fb40135579.png"},{"hero_id":75,"name":"狂野女猎手","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e90ed3f198064e9190426cf95986b02d.png"},{"hero_id":26807148,"name":"涤魂圣枪","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20191228/69428d235950432d8853b63e6f739370.png"}]
             */

            private String team_id;
            private String team_name;
            private String team_avatar;
            private String kill_count;
            private int big_dragon_count;
            private int small_dragon_count;
            private int tower_success_count;
            private int inhibitor_success_count;
            private boolean is_win;
            private String side;
            private List<PickListBean> pick_list;
            private List<BanListBean> ban_list;

            public String getKill_count() {
                return kill_count;
            }

            public void setKill_count(String kill_count) {
                this.kill_count = kill_count;
            }

            public String getTeam_id() {
                return team_id;
            }

            public void setTeam_id(String team_id) {
                this.team_id = team_id;
            }

            public String getTeam_name() {
                return team_name;
            }

            public void setTeam_name(String team_name) {
                this.team_name = team_name;
            }

            public String getTeam_avatar() {
                return team_avatar;
            }

            public void setTeam_avatar(String team_avatar) {
                this.team_avatar = team_avatar;
            }

            public int getBig_dragon_count() {
                return big_dragon_count;
            }

            public void setBig_dragon_count(int big_dragon_count) {
                this.big_dragon_count = big_dragon_count;
            }

            public int getSmall_dragon_count() {
                return small_dragon_count;
            }

            public void setSmall_dragon_count(int small_dragon_count) {
                this.small_dragon_count = small_dragon_count;
            }

            public int getTower_success_count() {
                return tower_success_count;
            }

            public void setTower_success_count(int tower_success_count) {
                this.tower_success_count = tower_success_count;
            }

            public int getInhibitor_success_count() {
                return inhibitor_success_count;
            }

            public void setInhibitor_success_count(int inhibitor_success_count) {
                this.inhibitor_success_count = inhibitor_success_count;
            }

            public boolean isIs_win() {
                return is_win;
            }

            public void setIs_win(boolean is_win) {
                this.is_win = is_win;
            }

            public String getSide() {
                return side;
            }

            public void setSide(String side) {
                this.side = side;
            }

            public List<PickListBean> getPick_list() {
                return pick_list;
            }

            public void setPick_list(List<PickListBean> pick_list) {
                this.pick_list = pick_list;
            }

            public List<BanListBean> getBan_list() {
                return ban_list;
            }

            public void setBan_list(List<BanListBean> ban_list) {
                this.ban_list = ban_list;
            }

            public static class PickListBean {
                /**
                 * hero_id : 10
                 * name : 沙漠皇帝
                 * avatar : https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png
                 */

                private int hero_id;
                private String name;
                private String avatar;

                public int getHero_id() {
                    return hero_id;
                }

                public void setHero_id(int hero_id) {
                    this.hero_id = hero_id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }
            }

            public static class BanListBean {
                /**
                 * hero_id : 10
                 * name : 沙漠皇帝
                 * avatar : https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png
                 */

                private int hero_id;
                private String name;
                private String avatar;

                public int getHero_id() {
                    return hero_id;
                }

                public void setHero_id(int hero_id) {
                    this.hero_id = hero_id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }
            }
        }
    }
}
