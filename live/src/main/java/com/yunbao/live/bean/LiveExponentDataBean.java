package com.yunbao.live.bean;

import java.util.List;

/**
 * @author wangjiang
 * @date 2020/10/1
 * Description:
 */
public class LiveExponentDataBean {

    /**
     * id : 117
     * start_time : 2020-10-17
     * league_id : 634
     * match_id : 66844
     * status : 2
     * viewnum : 2005
     * team_a_score : 3
     * team_b_score : 2
     * team_a_id : 670
     * team_b_id : 607
     * round_son_name :
     * battle_list : [{"battle_id":6684401,"match_id":66844,"duration":2022,"index":1,"economic_diff":[{"time":0,"diff":0},{"time":60,"diff":0},{"time":120,"diff":112},{"time":180,"diff":353},{"time":240,"diff":24},{"time":300,"diff":-455},{"time":360,"diff":686},{"time":420,"diff":482},{"time":480,"diff":101},{"time":540,"diff":294},{"time":600,"diff":230},{"time":660,"diff":-319},{"time":720,"diff":293},{"time":780,"diff":-203},{"time":840,"diff":-145},{"time":900,"diff":499},{"time":960,"diff":1187},{"time":1020,"diff":990},{"time":1080,"diff":1161},{"time":1140,"diff":454},{"time":1200,"diff":43},{"time":1260,"diff":-253},{"time":1320,"diff":29},{"time":1380,"diff":-303},{"time":1440,"diff":-3322},{"time":1500,"diff":-3300},{"time":1560,"diff":-4459},{"time":1620,"diff":-5168},{"time":1680,"diff":-4949},{"time":1740,"diff":-4974},{"time":1800,"diff":-4471},{"time":1860,"diff":-4831},{"time":1920,"diff":-5389},{"time":1980,"diff":-6836}],"xp_diff":[],"status":1,"start_time":0},{"battle_id":6684402,"match_id":66844,"duration":2166,"index":2,"economic_diff":[{"time":0,"diff":0},{"time":60,"diff":-20},{"time":120,"diff":50},{"time":180,"diff":211},{"time":240,"diff":-17},{"time":300,"diff":797},{"time":360,"diff":1131},{"time":420,"diff":821},{"time":480,"diff":741},{"time":540,"diff":1048},{"time":600,"diff":561},{"time":660,"diff":674},{"time":720,"diff":-39},{"time":780,"diff":1556},{"time":840,"diff":2016},{"time":900,"diff":2031},{"time":960,"diff":1588},{"time":1020,"diff":1864},{"time":1080,"diff":1935},{"time":1140,"diff":909},{"time":1200,"diff":1602},{"time":1260,"diff":1438},{"time":1320,"diff":252},{"time":1380,"diff":-609},{"time":1440,"diff":-1623},{"time":1500,"diff":-2565},{"time":1560,"diff":-3128},{"time":1620,"diff":-2997},{"time":1680,"diff":-3189},{"time":1740,"diff":-4088},{"time":1800,"diff":-3688},{"time":1860,"diff":-5425},{"time":1920,"diff":-4681},{"time":1980,"diff":-3663},{"time":2040,"diff":-3386},{"time":2100,"diff":-5255},{"time":2160,"diff":-7898}],"xp_diff":[],"status":1,"start_time":0},{"battle_id":6684403,"match_id":66844,"duration":1784,"index":3,"economic_diff":[{"time":0,"diff":0},{"time":60,"diff":0},{"time":120,"diff":118},{"time":180,"diff":427},{"time":240,"diff":161},{"time":300,"diff":293},{"time":360,"diff":372},{"time":420,"diff":540},{"time":480,"diff":1156},{"time":540,"diff":1566},{"time":600,"diff":1249},{"time":660,"diff":1657},{"time":720,"diff":1729},{"time":780,"diff":3712},{"time":840,"diff":3919},{"time":900,"diff":4137},{"time":960,"diff":5175},{"time":1020,"diff":5915},{"time":1080,"diff":6134},{"time":1140,"diff":6978},{"time":1200,"diff":7030},{"time":1260,"diff":7471},{"time":1320,"diff":7673},{"time":1380,"diff":7530},{"time":1440,"diff":7228},{"time":1500,"diff":7687},{"time":1560,"diff":8057},{"time":1620,"diff":8203},{"time":1680,"diff":9134},{"time":1740,"diff":9525}],"xp_diff":[],"status":1,"start_time":0},{"battle_id":6684404,"match_id":66844,"duration":1608,"index":4,"economic_diff":[{"time":0,"diff":0},{"time":60,"diff":0},{"time":120,"diff":-83},{"time":180,"diff":-45},{"time":240,"diff":-126},{"time":300,"diff":-193},{"time":360,"diff":505},{"time":420,"diff":982},{"time":480,"diff":2258},{"time":540,"diff":1730},{"time":600,"diff":967},{"time":660,"diff":-82},{"time":720,"diff":1196},{"time":780,"diff":1230},{"time":840,"diff":1320},{"time":900,"diff":1709},{"time":960,"diff":1082},{"time":1020,"diff":583},{"time":1080,"diff":1087},{"time":1140,"diff":950},{"time":1200,"diff":1079},{"time":1260,"diff":721},{"time":1320,"diff":1025},{"time":1380,"diff":1050},{"time":1440,"diff":-2300},{"time":1500,"diff":-2506},{"time":1560,"diff":-4889}],"xp_diff":[],"status":1,"start_time":0},{"battle_id":6684405,"match_id":66844,"duration":1522,"index":5,"economic_diff":[{"time":0,"diff":0},{"time":60,"diff":0},{"time":120,"diff":149},{"time":180,"diff":-436},{"time":240,"diff":-492},{"time":300,"diff":-445},{"time":360,"diff":367},{"time":420,"diff":158},{"time":480,"diff":-363},{"time":540,"diff":-364},{"time":600,"diff":-646},{"time":660,"diff":-842},{"time":720,"diff":-793},{"time":780,"diff":-1484},{"time":840,"diff":-1606},{"time":900,"diff":-3681},{"time":960,"diff":-2520},{"time":1020,"diff":-2015},{"time":1080,"diff":-2219},{"time":1140,"diff":-2101},{"time":1200,"diff":-2364},{"time":1260,"diff":-2518},{"time":1320,"diff":-3215},{"time":1380,"diff":-4545},{"time":1440,"diff":-5514},{"time":1500,"diff":-8746}],"xp_diff":[],"status":1,"start_time":0}]
     * startdate : 10-17
     * starttime : 18:00
     * league_name : S10 世界总决赛
     * battle_current_index : 5
     * battle_current_id : 6684405
     * battle_duration : 1522
     * team_a_name : TES
     * team_a_logo : https://qn.feijing88.com/egame/lol/team/fcbf7279a5638b6bc9605ab1a1991798.png
     * team_b_name : FNC
     * team_b_logo : https://qn.feijing88.com/egame/lol/team/fbc29df15f99f56a1ae7e193b709beb2.png
     * league_cat : 英雄联盟
     * data : [{"source":"aviagaming","options":[{"team_id":"670","init_handicap":1.114,"odds":1.052},{"team_id":"607","init_handicap":6.452,"odds":8.009}],"jumpurl":"https://www.ob145.com/"},{"source":"bet365","options":[{"team_id":"670","init_handicap":1.11,"odds":1.07},{"team_id":"607","init_handicap":6,"odds":7.5}],"jumpurl":"https://www.ob145.com/"},{"source":"雷竞技","options":[{"team_id":"670","init_handicap":1.16,"odds":1.07},{"team_id":"607","init_handicap":5.27,"odds":8.52}],"jumpurl":"https://www.ob145.com/"},{"source":"雷火","options":[{"team_id":"670","init_handicap":1.03,"odds":1.03},{"team_id":"607","init_handicap":10.09,"odds":10.09}],"jumpurl":"https://www.ob145.com/"},{"source":"雷火","options":[{"team_id":"607","init_handicap":1.52,"odds":1.52},{"team_id":"670","init_handicap":2.38,"odds":2.38}],"jumpurl":"https://www.ob145.com/"},{"source":"uwinmax","options":[{"team_id":"670","init_handicap":1.1,"odds":1.06},{"team_id":"607","init_handicap":6.69,"odds":8.58}],"jumpurl":"https://www.ob145.com/"}]
     */

    private String id;  //id
    private String start_time; //比赛开始时间
    private String league_id;  //联赛ID
    private String match_id; //比赛ID
    private String status; //比赛状态 0:未开始 1:进行中 2:已结束 3:已延期 4:已删除
    private int viewnum; // 观看次数
    private String team_a_score; //主队得分
    private String team_b_score; //客队得分
    private String team_a_id; //主队ID
    private String team_b_id; //客队ID
    private String round_son_name; //子轮次名次
    private String battle_list; //
    private String startdate; //比赛开始日期
    private String starttime; //比赛开始时间
    private String league_name; //联赛名称
    private int battle_current_index; //当前对局局数
    private int battle_current_id; //当前对局id
    private int battle_duration; //当前对局时长
    private String team_a_name;  //主队名
    private String team_a_logo;  //主队logo
    private String team_b_name;  //客队名
    private String team_b_logo;  //客队LOGO
    private String league_cat;   //联赛分类
    private List<DataBean> data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getLeague_id() {
        return league_id;
    }

    public void setLeague_id(String league_id) {
        this.league_id = league_id;
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getViewnum() {
        return viewnum;
    }

    public void setViewnum(int viewnum) {
        this.viewnum = viewnum;
    }

    public String getTeam_a_score() {
        return team_a_score;
    }

    public void setTeam_a_score(String team_a_score) {
        this.team_a_score = team_a_score;
    }

    public String getTeam_b_score() {
        return team_b_score;
    }

    public void setTeam_b_score(String team_b_score) {
        this.team_b_score = team_b_score;
    }

    public String getTeam_a_id() {
        return team_a_id;
    }

    public void setTeam_a_id(String team_a_id) {
        this.team_a_id = team_a_id;
    }

    public String getTeam_b_id() {
        return team_b_id;
    }

    public void setTeam_b_id(String team_b_id) {
        this.team_b_id = team_b_id;
    }

    public String getRound_son_name() {
        return round_son_name;
    }

    public void setRound_son_name(String round_son_name) {
        this.round_son_name = round_son_name;
    }

    public String getBattle_list() {
        return battle_list;
    }

    public void setBattle_list(String battle_list) {
        this.battle_list = battle_list;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getLeague_name() {
        return league_name;
    }

    public void setLeague_name(String league_name) {
        this.league_name = league_name;
    }

    public int getBattle_current_index() {
        return battle_current_index;
    }

    public void setBattle_current_index(int battle_current_index) {
        this.battle_current_index = battle_current_index;
    }

    public int getBattle_current_id() {
        return battle_current_id;
    }

    public void setBattle_current_id(int battle_current_id) {
        this.battle_current_id = battle_current_id;
    }

    public int getBattle_duration() {
        return battle_duration;
    }

    public void setBattle_duration(int battle_duration) {
        this.battle_duration = battle_duration;
    }

    public String getTeam_a_name() {
        return team_a_name;
    }

    public void setTeam_a_name(String team_a_name) {
        this.team_a_name = team_a_name;
    }

    public String getTeam_a_logo() {
        return team_a_logo;
    }

    public void setTeam_a_logo(String team_a_logo) {
        this.team_a_logo = team_a_logo;
    }

    public String getTeam_b_name() {
        return team_b_name;
    }

    public void setTeam_b_name(String team_b_name) {
        this.team_b_name = team_b_name;
    }

    public String getTeam_b_logo() {
        return team_b_logo;
    }

    public void setTeam_b_logo(String team_b_logo) {
        this.team_b_logo = team_b_logo;
    }

    public String getLeague_cat() {
        return league_cat;
    }

    public void setLeague_cat(String league_cat) {
        this.league_cat = league_cat;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * source : aviagaming
         * options : [{"team_id":"670","init_handicap":1.114,"odds":1.052},{"team_id":"607","init_handicap":6.452,"odds":8.009}]
         * jumpurl : https://www.ob145.com/
         */

        private String source;
        private String jumpurl;
        private List<OptionsBean> options;

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getJumpurl() {
            return jumpurl;
        }

        public void setJumpurl(String jumpurl) {
            this.jumpurl = jumpurl;
        }

        public List<OptionsBean> getOptions() {
            return options;
        }

        public void setOptions(List<OptionsBean> options) {
            this.options = options;
        }

        public static class OptionsBean {
            /**
             * team_id : 670
             * init_handicap : 1.114
             * odds : 1.052
             */

            private String team_id;
            private double init_handicap;
            private double odds;

            public String getTeam_id() {
                return team_id;
            }

            public void setTeam_id(String team_id) {
                this.team_id = team_id;
            }

            public double getInit_handicap() {
                return init_handicap;
            }

            public void setInit_handicap(double init_handicap) {
                this.init_handicap = init_handicap;
            }

            public double getOdds() {
                return odds;
            }

            public void setOdds(double odds) {
                this.odds = odds;
            }
        }
    }
}
