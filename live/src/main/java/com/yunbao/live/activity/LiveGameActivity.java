package com.yunbao.live.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.fastjson.JSON;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.Constants;
import com.yunbao.common.HtmlConfig;
import com.yunbao.common.adapter.ViewPagerAdapter;
import com.yunbao.common.bean.GameLolMatchBean;
import com.yunbao.common.bean.MainRecommendBean;
import com.yunbao.common.dialog.LiveChargeDialogFragment;
import com.yunbao.common.http.CommonHttpConsts;
import com.yunbao.common.http.CommonHttpUtil;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.http.HttpCallbackObject;
import com.yunbao.common.pay.PayCallback;
import com.yunbao.common.pay.PayPresenter;
import com.yunbao.common.utils.AppLog;
import com.yunbao.common.utils.DialogUitl;
import com.yunbao.common.utils.DpUtil;
import com.yunbao.common.utils.L;
import com.yunbao.common.utils.MD5Util;
import com.yunbao.common.utils.RandomUtil;
import com.yunbao.common.utils.RouteUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.common.views.AbsViewHolder;
import com.yunbao.game.event.GameWindowChangedEvent;
import com.yunbao.game.event.OpenGameChargeEvent;
import com.yunbao.live.R;
import com.yunbao.live.adapter.LiveBattleViewHolder;
import com.yunbao.live.adapter.LiveRoomScrollAdapter;
import com.yunbao.live.bean.LiveBean;
import com.yunbao.live.bean.LiveChatUserBean;
import com.yunbao.live.bean.LiveNewChatBean;
import com.yunbao.live.dialog.LiveGiftDialogFragment;
import com.yunbao.live.dialog.LiveGoodsDialogFragment;
import com.yunbao.live.event.LinkMicTxAccEvent;
import com.yunbao.live.event.LiveRoomChangeEvent;
import com.yunbao.live.event.LiveRoomInfoEvent;
import com.yunbao.live.event.MatchEvent;
import com.yunbao.live.http.LiveHttpConsts;
import com.yunbao.live.http.LiveHttpUtil;
import com.yunbao.live.http.WsManager;
import com.yunbao.live.presenter.LiveRoomCheckLivePresenter2;
import com.yunbao.live.socket.SocketChatUtil;
import com.yunbao.live.views.LiveAudienceViewHolder;
import com.yunbao.live.views.LiveGameAnalysisViewHolder;
import com.yunbao.live.views.LiveGameChatViewHolder;
import com.yunbao.live.views.LiveGameDataViewHolder;
import com.yunbao.live.views.LiveGameExponentViewHolder;
import com.yunbao.live.views.LiveGamePlayerViewHolder;
import com.yunbao.live.views.LivePlayKsyViewHolder;
import com.yunbao.live.views.LivePlayTxViewHolder;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by cxf on 2018/10/10.
 */

public class LiveGameActivity extends LiveActivity {

    private static final String TAG = "LiveAudienceActivity";
    private LiveBattleViewHolder mLiveBattleViewHolder;
    private LivePlayKsyViewHolder livePlayKsyViewHolder;
    private final static int ANALYSIS_RETURN_GAME_NUMBER = 1;
    private WsManager wsManager;
    private LinearLayout mLiveLlMatchInfo;
    private View playContainer;

    public static void forward(Context context, LiveBean liveBean, int liveType, int liveTypeVal, String key, int position, int liveSdk) {
        Intent intent = new Intent(context, LiveGameActivity.class);
        intent.putExtra(Constants.LIVE_BEAN, liveBean);
        intent.putExtra(Constants.LIVE_TYPE, liveType);
        intent.putExtra(Constants.LIVE_TYPE_VAL, liveTypeVal);
        intent.putExtra(Constants.LIVE_KEY, key);
        intent.putExtra(Constants.LIVE_POSITION, position);
        intent.putExtra(Constants.LIVE_SDK, liveSdk);
        context.startActivity(intent);
    }

    private boolean mUseScroll = true;
    private String mKey;
    private int mPosition;
    private RecyclerView mRecyclerView;
    private LiveRoomScrollAdapter mRoomScrollAdapter;
    private View mMainContentView;
    private ViewPager mViewPager;
    private LivePlayTxViewHolder mLivePlayViewHolder;
    private LiveAudienceViewHolder mLiveAudienceViewHolder;
    private LiveGameDataViewHolder mLiveGameDataViewHolder;
    private LiveGameExponentViewHolder mLiveExponentViewHolder;
    private LiveGamePlayerViewHolder mLiveGamePlayerViewHolder;
    private LiveGameAnalysisViewHolder mLiveGameAnalysisViewHolder;
    private boolean mEnd;
    private boolean mCoinNotEnough;//余额不足
    private LiveRoomCheckLivePresenter2 mCheckLivePresenter;
    private boolean mLighted;
    private PayPresenter mPayPresenter;
    private MagicIndicator mIndicator;

    private List<FrameLayout> mViewList;
    private String[] mBottomTitles = {"聊天"};
    private AbsViewHolder[] mAbsViewHolders;
    private MainRecommendBean mainRecommendBean;
    private GameLolMatchBean mMatchInfo; //赛事
    private TextView mTvLiveTitle;
    private TextView mTvGameTitle;
    public LinearLayout mRlTitle;

    @Override
    protected void getInentParams() {
        Intent intent = getIntent();
        mLiveSDK = intent.getIntExtra(Constants.LIVE_SDK, Constants.LIVE_SDK_KSY);
        mMatchInfo = (GameLolMatchBean) intent.getSerializableExtra(Constants.LIVE_MATCH_INFO);
        mainRecommendBean = (MainRecommendBean) intent.getSerializableExtra(Constants.LIVE_RECOMMEND_DATA);
        L.e(TAG, "直播sdk----->" + (mLiveSDK == Constants.LIVE_SDK_KSY ? "金山云" : "腾讯云"));
        mKey = intent.getStringExtra(Constants.LIVE_KEY);
        if (TextUtils.isEmpty(mKey)) {
            mUseScroll = false;
        }
        mPosition = intent.getIntExtra(Constants.LIVE_POSITION, 0);
        mLiveType = intent.getIntExtra(Constants.LIVE_TYPE, Constants.LIVE_TYPE_NORMAL);
        mLiveTypeVal = intent.getIntExtra(Constants.LIVE_TYPE_VAL, 0);
        mLiveBean = intent.getParcelableExtra(Constants.LIVE_BEAN);

        if (mainRecommendBean != null && !TextUtils.isEmpty(mainRecommendBean.getMatch_id()) && !"0".equals(mainRecommendBean.getMatch_id())) {  //不等于0 说明关联比赛
            getMatchData(mainRecommendBean.getMatch_id());
//            mBottomTitles = new String[]{"主播", "数据", "指数", "阵容", "选手", "分析"};
            mBottomTitles = new String[]{"主播", "数据", "指数", "选手", "分析"};
        }

        if (mMatchInfo != null) {
//            mBottomTitles = new String[]{"主播", "数据", "指数", "阵容", "选手", "分析"};
            mBottomTitles = new String[]{"主播", "数据", "指数", "选手", "分析"};
        }
    }


    private void getMatchData(String match_id) {
        LiveHttpUtil.getMatchInfo(match_id, new HttpCallbackObject() {
            @Override
            public void onSuccess(int code, String msg, Object info) {
                mMatchInfo = JSON.parseObject(info.toString(), GameLolMatchBean.class);
                matchStatus();
            }
        });
    }

    private void initMatchView() {
        mLiveLlMatchInfo = findViewById(R.id.live_ll_match_info);

        mLiveLlMatchInfo.setVisibility(View.VISIBLE);

        TextView liveDateInfo = findViewById(R.id.live_tv_date_info);
        ImageView teamAIcon = findViewById(R.id.live_iv_team_one_icon);
        ImageView teamBIcon = findViewById(R.id.live_iv_team_two_icon);
        TextView liveStats = findViewById(R.id.live_view_stats);
        TextView liveTextStatus = findViewById(R.id.live_tv_txt_status);
        TextView teamAName = findViewById(R.id.live_iv_team_one_name);
        TextView teamBName = findViewById(R.id.live_iv_team_two_name);
        LinearLayout liveStatus = findViewById(R.id.ll_live_status);
        findViewById(R.id.live_tv_living).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLiveLlMatchInfo.setVisibility(View.GONE);
                if (mLivePlayViewHolder != null) {
                    //TODO 暂时固定
                    mLivePlayViewHolder.setLiveId(mMatchInfo.getMatch_id());
                    mLivePlayViewHolder.play("http://tx2play1.douyucdn.cn/live/7596577rnqGac34w.flv?uuid=");
                }
            }
        });

        findViewById(R.id.live_tv_animation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO
            }
        });

        String currentStr = "";
        if (mMatchInfo.getBattle_current_index() == 1) {
            currentStr = "第一局";
        } else if (mMatchInfo.getBattle_current_index() == 2) {
            currentStr = "第二局";
        } else if (mMatchInfo.getBattle_current_index() == 3) {
            currentStr = "第三局";
        } else if (mMatchInfo.getBattle_current_index() == 4) {
            currentStr = "第四局";
        } else if (mMatchInfo.getBattle_current_index() == 5) {
            currentStr = "第五局";
        }

//        mTvLiveTitle.setText(mMatchInfo.getLeague_name())
        mTvGameTitle.setText(mMatchInfo.getLeague_name());
        liveDateInfo.setText(MessageFormat.format("{0} {1} {2}", mMatchInfo.getStartdate(), mMatchInfo.getStarttime(), currentStr));
        setImage(teamAIcon, mMatchInfo.getTeam_a_logo());
        setImage(teamBIcon, mMatchInfo.getTeam_b_logo());
        if (mMatchInfo.getStatus() == 0) {
            liveTextStatus.setText("未开赛");
            liveStatus.setVisibility(View.GONE);
            liveStats.setText("VS");
        } else if (mMatchInfo.getStatus() == 1) {
            liveTextStatus.setText("");
            liveStatus.setVisibility(View.VISIBLE);
            liveStats.setText(MessageFormat.format("{0} - {1}", mMatchInfo.getTeam_a_score(), mMatchInfo.getTeam_b_score()));
        } else if (mMatchInfo.getStatus() == 2) {
            liveTextStatus.setText("比赛结束");
            liveStatus.setVisibility(View.GONE);
            liveStats.setText(MessageFormat.format("{0} - {1}", mMatchInfo.getTeam_a_score(), mMatchInfo.getTeam_b_score()));
        }

        teamAName.setText(mMatchInfo.getTeam_a_name());
        teamBName.setText(mMatchInfo.getTeam_b_name());
    }

    private void setImage(ImageView iv, String ivUrl) {
        Glide.with(mContext)
                .load(ivUrl)
                .apply(options)
                .into(iv);
    }

    private RequestOptions options = new RequestOptions()
            .placeholder(R.mipmap.icon_live_big_crown)
            .error(R.mipmap.icon_live_big_crown);

    private boolean isUseScroll() {
        return mUseScroll && CommonAppConfig.LIVE_ROOM_SCROLL;
    }

    @Override
    public <T extends View> T findViewById(@IdRes int id) {
        if (isUseScroll()) {
            if (mMainContentView != null) {
                return mMainContentView.findViewById(id);
            }
        }
        return super.findViewById(id);
    }

    public void showFullScreen() {
        setConfigDisplay(playContainer, false);
        mLivePlayViewHolder.changeWaterMark();
    }


    @Override
    protected int getLayoutId() {
        if (isUseScroll()) {
            return R.layout.activity_live_audience_2;
        }
        return R.layout.activity_live_game_audience;
    }

    public void setScrollFrozen(boolean frozen) {
        if (mRecyclerView != null) {
            mRecyclerView.setLayoutFrozen(frozen);
        }
    }

    @Override
    protected void main() {
        super.main();
        ImmersionBar.with(this).statusBarDarkFont(false).titleBar(findViewById(R.id.rl_title)).navigationBarColor(R.color.white).navigationBarDarkIcon(true).init();
        mTvLiveTitle = findViewById(R.id.live_tv_live_title);
        mTvGameTitle = findViewById(R.id.tv_game_title);
        mViewPager = findViewById(R.id.viewPager);
        mIndicator = findViewById(R.id.indicator);
        mRlTitle = findViewById(R.id.rl_title);
        playContainer = findViewById(R.id.play_container);

        findViewById(R.id.live_iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    finish();
                    return;
                }
                showFullScreen();
            }
        });
        findViewById(R.id.iv_game_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (mMatchInfo != null && mainRecommendBean == null) {
            initMatchView();
        }
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        if (isUseScroll()) {
            mRecyclerView = super.findViewById(R.id.recyclerView);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            mMainContentView = LayoutInflater.from(mContext).inflate(R.layout.activity_live_audience, null, false);
        }
//        if (mLiveSDK == Constants.LIVE_SDK_TX || isUseScroll()) {
        //腾讯视频播放器
        mLivePlayViewHolder = new LivePlayTxViewHolder(mContext, (ViewGroup) playContainer);
//        } else {
//            //金山云播放器
//            mLivePlayViewHolder = new LivePlayKsyViewHolder(mContext, (ViewGroup) findViewById(R.id.play_container));
//        }

        mAbsViewHolders = new AbsViewHolder[mBottomTitles.length];
        mLivePlayViewHolder.addToParent();
        mLivePlayViewHolder.subscribeActivityLifeCycle();

        matchStatus();
        setViewPage();
        mViewPager.setCurrentItem(0);

        if (mainRecommendBean != null && mainRecommendBean.getPull() != null) {
            mTvLiveTitle.setText(mainRecommendBean.getTitle());
            setLiveRoomData(mainRecommendBean);
            enterRoom();
        }

        if (mMatchInfo != null && !TextUtils.isEmpty(mMatchInfo.getMatch_id())) {
            mTvGameTitle.setText(mMatchInfo.getLeague_name());
            enterRoom();
        }
        loadPageData(0);
    }

    private void matchStatus() {
        if (mainRecommendBean != null && mMatchInfo == null) {
            findViewById(R.id.live_ll_living_indicator).setVisibility(View.VISIBLE);
            mIndicator.setVisibility(View.GONE);
        } else {
            findViewById(R.id.live_ll_living_indicator).setVisibility(View.GONE);
            mIndicator.setVisibility(View.VISIBLE);
        }
    }

    private void setViewPage() {

        mViewList = new ArrayList<>();
        for (int i = 0; i < mBottomTitles.length; i++) {
            FrameLayout frameLayout = new FrameLayout(mContext);
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            mViewList.add(frameLayout);
        }

        mViewPager.setAdapter(new ViewPagerAdapter(mViewList));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                loadPageData(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mIndicator = findViewById(R.id.indicator);
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {

            @Override
            public int getCount() {
                return mBottomTitles.length;
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                return getIndicatorTitleView(context, mBottomTitles, index);
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_MATCH_EDGE);
                linePagerIndicator.setXOffset(DpUtil.dp2px(5));
                linePagerIndicator.setRoundRadius(DpUtil.dp2px(2));
                linePagerIndicator.setColors(ContextCompat.getColor(mContext, R.color.live_ff5116));
                return linePagerIndicator;
            }
        });
        mIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mIndicator, mViewPager);
    }


    private void loadPageData(int position) {
        if (mAbsViewHolders == null) {
            return;
        }
        AbsViewHolder vh = mAbsViewHolders[position];
        if (vh == null) {
            if (mViewList != null && position < mViewList.size()) {
                FrameLayout parent = mViewList.get(position);
                if (parent == null) {
                    return;
                }
                if (mMatchInfo != null) {
                    if (position == 0) {
                        mLiveChatViewHolder = new LiveGameChatViewHolder(mContext, parent, LiveGameChatViewHolder.TYPE_GAME);
                        vh = mLiveChatViewHolder;
                    } else if (position == 1) {
                        mLiveGameDataViewHolder = new LiveGameDataViewHolder(mContext, parent);
                        vh = mLiveGameDataViewHolder;
                    } else if (position == 2) {
                        mLiveExponentViewHolder = new LiveGameExponentViewHolder(mContext, parent);
                        vh = mLiveExponentViewHolder;
                    }
//                    else if (position == 3) {
//                        mLiveBattleViewHolder = new LiveBattleViewHolder(mContext, parent);
//                        vh = mLiveBattleViewHolder;
//                    }
                    else if (position == 3) {
                        mLiveGamePlayerViewHolder = new LiveGamePlayerViewHolder(mContext, parent);
                        vh = mLiveGamePlayerViewHolder;
                    } else if (position == 4) {
                        mLiveGameAnalysisViewHolder = new LiveGameAnalysisViewHolder(mContext, parent);
                        vh = mLiveGameAnalysisViewHolder;
                    } else {
                        mLiveAudienceViewHolder = new LiveAudienceViewHolder(mContext, parent);
                        vh = mLiveAudienceViewHolder;
                    }
                } else {
                    mLiveChatViewHolder = new LiveGameChatViewHolder(mContext, parent, LiveGameChatViewHolder.TYPE_GAME);
                    vh = mLiveChatViewHolder;
                }
                mAbsViewHolders[position] = vh;
                vh.addToParent();
                vh.subscribeActivityLifeCycle();
            }
        }

        if (vh instanceof LivePlayKsyViewHolder) {
            ((LivePlayKsyViewHolder) vh).play("https://esportsapi.feijing88.com/live/v2/?v=1d02467eaf7ab044e0170fadaff5fa8bf0f9483dccbb92074dc275d0321ea3c9ab027dda3b89ad16d1573e0c3fc077d0f655583ded16ae543780eb59dc178501fc553778a6c3eed169e513be5559a4794babed9b4298e49902377b88c076809a");
        } else if (vh instanceof LiveGameDataViewHolder) {
            mLiveGameDataViewHolder.loadData(mMatchInfo, 1);
        } else if (vh instanceof LiveGameExponentViewHolder) {
            mLiveExponentViewHolder.loadData(mMatchInfo);
        }
//        else if (vh instanceof LiveBattleViewHolder) {
//            mLiveBattleViewHolder.loadData(mMatchInfo);
//        }
        else if (vh instanceof LiveGamePlayerViewHolder) {
            mLiveGamePlayerViewHolder.loadData(mMatchInfo);
        } else if (vh instanceof LiveGameAnalysisViewHolder) {
            mLiveGameAnalysisViewHolder.loadData(mMatchInfo);
        } else if (vh instanceof LiveGameChatViewHolder) {
            if (mMatchInfo != null && mainRecommendBean == null) {
                mLiveChatViewHolder.isMatch();
            }
            if (mainRecommendBean != null) {
                mLiveChatViewHolder.setData(mainRecommendBean);
                mLiveChatViewHolder.setLiveInfo(mLiveUid, mStream);
            }
        }
    }

    public void setInPutEnable(boolean b) {
        if (mLiveChatViewHolder == null) {
            loadPageData(0);
        }
        if (b) {
            mTvLiveTitle.setText(mainRecommendBean == null ? "" : mainRecommendBean.getTitle());
        } else {
            mTvLiveTitle.setText("");
        }
        mLiveChatViewHolder.setInPutEnable(b);
    }


    protected IPagerTitleView getIndicatorTitleView(Context context, String[] titles, final int index) {
        SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
        simplePagerTitleView.setNormalColor(ContextCompat.getColor(mContext, R.color.live_2B2626));
        simplePagerTitleView.setSelectedColor(ContextCompat.getColor(mContext, R.color.live_ff5116));
        simplePagerTitleView.setText(titles[index]);
        simplePagerTitleView.setTextSize(13);
        simplePagerTitleView.getPaint().setFakeBoldText(true);
        simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager != null) {
                    mViewPager.setCurrentItem(index);
                }
            }
        });
        return simplePagerTitleView;
    }


    public void scrollNextPosition() {
        if (mRoomScrollAdapter != null) {
            mRoomScrollAdapter.scrollNextPosition();
        }
    }


    private void setLiveRoomData(MainRecommendBean liveBean) {
        mLiveUid = liveBean.getUid();
        mStream = liveBean.getStream();
//        mLivePlayViewHolder.setCover(liveBean.getThumb());
        mLivePlayViewHolder.setPlayMode(liveBean.getAnyway(), liveBean.getIsvideo());
        mLivePlayViewHolder.setLiveId(mLiveUid);
        String mUrl = mainRecommendBean.getPull();
        long l = System.currentTimeMillis() / 1000;
        String url = mUrl + "?txSecret=" + MD5Util.getMD5("02019d98d6084be641cbf67ce3aa65fc"
                + mStream + Long.toHexString(l))
                + "&txTime=" + Long.toHexString(l);
        mLivePlayViewHolder.play(url);
//        mLivePlayViewHolder.setNoAnchorImage(mainRecommendBean.getThumb());
//        mLivePlayViewHolder.play(liveBean.getPull());
//        mLiveAudienceViewHolder.setLiveInfo(mLiveUid, mStream);
//        mLiveRoomViewHolder.setAvatar(liveBean.getAvatar());
//        mLiveRoomViewHolder.setAnchorLevel(liveBean.getLevelAnchor());
//        mLiveRoomViewHolder.setName(liveBean.getUserNiceName());
//        mLiveRoomViewHolder.setRoomNum(liveBean.getLiangNameTip());
//        mLiveRoomViewHolder.setTitle(liveBean.getTitle());
//        mLiveLinkMicPkPresenter.setLiveUid(mLiveUid);
//        mLiveLinkMicPresenter.setLiveUid(mLiveUid);
//        mLiveAudienceViewHolder.setShopOpen(liveBean.getIsshop() == 1);
    }

    private void clearRoomData() {
        if (mSocketClient != null) {
            mSocketClient.disConnect();
        }
        mSocketClient = null;
        if (mLivePlayViewHolder != null) {
            mLivePlayViewHolder.stopPlay();
        }

        if (mLiveRoomViewHolder != null) {
            mLiveRoomViewHolder.clearData();
        }
        if (mGamePresenter != null) {
            mGamePresenter.clearGame();
        }
        if (mLiveEndViewHolder != null) {
            mLiveEndViewHolder.removeFromParent();
        }
        if (mLiveLinkMicPresenter != null) {
            mLiveLinkMicPresenter.clearData();
        }
        if (mLiveLinkMicAnchorPresenter != null) {
            mLiveLinkMicAnchorPresenter.clearData();
        }
        if (mLiveLinkMicPkPresenter != null) {
            mLiveLinkMicPkPresenter.clearData();
        }
        setPkBgVisible(false);
    }

    /**
     * 进入主播或者赛事直播房间socket
     */
    private void enterRoom() {
        if (mainRecommendBean != null) {
            AppLog.e("enterRoom");
            if (!TextUtils.isEmpty(mLiveUid)) {
                LiveHttpUtil.enterRoom(mLiveUid, mStream, new HttpCallback() {
                    @Override
                    public void onSuccess(int code, String msg, String[] info) {
                        if (code == 0 && info.length > 0) {

                        }
                    }
                });
            }
            wsManager = new WsManager.Builder(getBaseContext(), true, false).client(
                    new OkHttpClient().newBuilder()
                            .pingInterval(15, TimeUnit.SECONDS)
                            .retryOnConnectionFailure(true)
                            .build())
                    .needReconnect(true)
                    .wsUrl(CommonAppConfig.WEBSOCKET_CHAT).build();
            if (mLiveUid != null) {
                wsManager.setLiveId(mLiveUid);
            }
            wsManager.setOnReceiveListener(new WsManager.OnReceiveListener() {
                @Override
                public void onMessage(String message) {
                    if (TextUtils.isEmpty(mTvLiveTitle.getText())) {
                        setInPutEnable(true);
                    }
                    sendMsg(message);
                }

                @Override
                public void onOpen() {
                    mWebSocketClient = wsManager.getWebSocket();
                }
            });
            wsManager.startConnect();
        }

    }

    /**
     * 收到的socket数据
     *
     * @param message
     */
    private void sendMsg(String message) {
        if (mainRecommendBean != null) {
            try {
                JSONObject object = new org.json.JSONObject(message);
                int code = object.getInt("code");
                String msg = object.getString("msg");
                if (code == 0) { //0 成功
                    final LiveNewChatBean liveNewChatBean = new Gson().fromJson(object.getString("data"), LiveNewChatBean.class);
                    List<LiveNewChatBean> logs = liveNewChatBean.getLogs();
                    if (logs != null) {
                        for (LiveNewChatBean log : logs) {
                            onNewChat(log);
                        }
                    }
                    onNewChat(liveNewChatBean);
                } else if (code == 2000) {
//                   下播 {"code":2000,"msg":"","data":{}}
//                    ToastUtil.show("聊天室已关闭!");
                    if (mLivePlayViewHolder != null) {
                        mLivePlayViewHolder.stopPlay();
                        mLivePlayViewHolder.showCover(false);
                        setInPutEnable(false);
                    }
                } else if (code == 2001 || code == 1004) {
//                    {"code":2001,"msg":"被禁言！","data":{"touid":"1","tousername":"xxx"}}
                    if (object.has("data")) {
                        LiveChatUserBean liveChatUserBean = new Gson().fromJson(object.getString("data"), LiveChatUserBean.class);
                        String uid = liveChatUserBean.getUid() == null ? liveChatUserBean.getTouid() : liveChatUserBean.getUid();
                        //是自己才弹框提示
                        if (!TextUtils.isEmpty(uid)) {
                            if (uid.equals(CommonAppConfig.getInstance().getUid())) {
                                if (!isFinishing()) {
                                    DialogUitl.showLogoTipDialog(LiveGameActivity.this, getString(R.string.dialog_tip), getString(R.string.live_you_are_shut_contact_customer_service), true,
                                            true, true, false);
                                }
                            }
                            LiveNewChatBean liveNewChatBean = new LiveNewChatBean();
                            liveNewChatBean.setType("2001");
                            liveNewChatBean.setMsg(msg);
                            liveNewChatBean.setUser(liveChatUserBean);
                            onNewChat(liveNewChatBean);
                        }
                    } else {
//                        ToastUtil.show(msg);
                        DialogUitl.showLogoTipDialog(LiveGameActivity.this, getString(R.string.dialog_tip), msg, true,
                                true, true, false);
                    }

                } else if (code == 2002) {
//                    {"code":2002,"msg":"被设为管理员！","data":{"touid":"1","tousername":"xxx"}}
                    LiveChatUserBean liveChatUserBean = new Gson().fromJson(object.getString("data"), LiveChatUserBean.class);
                    String uid = liveChatUserBean.getUid() == null ? liveChatUserBean.getTouid() : liveChatUserBean.getUid();
                    if (!TextUtils.isEmpty(uid)) {
                        //是自己才弹框提示
                        if (uid.equals(CommonAppConfig.getInstance().getUid())) {
                            if (!TextUtils.isEmpty(msg)) {
                                ToastUtil.show(msg);
                            }
                        }
                        LiveNewChatBean liveNewChatBean = new LiveNewChatBean();
                        liveNewChatBean.setType("2002");
                        liveNewChatBean.setMsg(msg);
                        liveNewChatBean.setUser(liveChatUserBean);
                        onNewChat(liveNewChatBean);

                    }

                } else {
                    if (!TextUtils.isEmpty(msg)) {
                        // TODO: 2020/11/13
//                        ToastUtil.show(msg);
                        AppLog.e(msg);
                    }
                }
            } catch (JSONException e) {
                AppLog.e(e.toString());
            }
        }
    }

    /**
     * 打开礼物窗口
     */
    public void openGiftWindow() {
        if (TextUtils.isEmpty(mLiveUid) || TextUtils.isEmpty(mStream)) {
            return;
        }
        LiveGiftDialogFragment fragment = new LiveGiftDialogFragment();
        fragment.setLifeCycleListener(this);
        fragment.setLiveGuardInfo(mLiveGuardInfo);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.LIVE_UID, mLiveUid);
        bundle.putString(Constants.LIVE_STREAM, mStream);
        fragment.setArguments(bundle);
        fragment.show(((LiveGameActivity) mContext).getSupportFragmentManager(), "LiveGiftDialogFragment");
    }

    /**
     * 结束观看
     */
    private void endPlay() {
        leaveRoom();
        if (mEnd) {
            return;
        }
        mEnd = true;
        //断开socket
        if (mSocketClient != null) {
            mSocketClient.disConnect();
        }
        mSocketClient = null;
        //结束播放
        if (mLivePlayViewHolder != null) {
            mLivePlayViewHolder.release();
        }
        mLivePlayViewHolder = null;
        release();
    }

    @Override
    protected void release() {
        if (mSocketClient != null) {
            mSocketClient.disConnect();
        }
        if (mPayPresenter != null) {
            mPayPresenter.release();
        }
        mPayPresenter = null;
        LiveHttpUtil.cancel(LiveHttpConsts.CHECK_LIVE);
        LiveHttpUtil.cancel(LiveHttpConsts.ENTER_ROOM);
        LiveHttpUtil.cancel(LiveHttpConsts.ROOM_CHARGE);
        CommonHttpUtil.cancel(CommonHttpConsts.GET_BALANCE);
        super.release();
        if (mRoomScrollAdapter != null) {
            mRoomScrollAdapter.release();
        }
        mRoomScrollAdapter = null;
    }

    /**
     * 观众收到直播结束消息
     */
    @Override
    public void onLiveEnd() {
        super.onLiveEnd();
        endPlay();
        if (mViewPager != null) {
            if (mViewPager.getCurrentItem() != 1) {
                mViewPager.setCurrentItem(1, false);
            }
//            mViewPager.setCanScroll(false);
        }
//        if (mLiveEndViewHolder == null) {
//            mLiveEndViewHolder = new LiveEndViewHolder(mContext, mSecondPage);
//            mLiveEndViewHolder.subscribeActivityLifeCycle();
//            mLiveEndViewHolder.addToParent();
//        }
        mLiveEndViewHolder.showData(mLiveBean, mStream);
        if (isUseScroll()) {
            if (mRecyclerView != null) {
                mRecyclerView.setLayoutFrozen(true);
            }
        }
    }


    /**
     * 观众收到踢人消息
     */
    @Override
    public void onKick(String touid) {
        if (!TextUtils.isEmpty(touid) && touid.equals(CommonAppConfig.getInstance().getUid())) {//被踢的是自己
            exitLiveRoom();
            ToastUtil.show(WordUtil.getString(R.string.live_kicked_2));
        }
    }

    /**
     * 观众收到禁言消息
     */
    @Override
    public void onShutUp(String touid, String content) {
        if (!TextUtils.isEmpty(touid) && touid.equals(CommonAppConfig.getInstance().getUid())) {
            DialogUitl.showSimpleTipDialog(mContext, content);
        }
    }

    @Override
    public void onBackPressed() {
        if (!mEnd && !canBackPressed()) {
            return;
        }
        exitLiveRoom();
    }

    /**
     * 退出直播间
     */
    public void exitLiveRoom() {
        endPlay();
        super.onBackPressed();
    }


    @Override
    protected void onDestroy() {
        //离开直播间
        leaveRoom();
        if (mLiveAudienceViewHolder != null) {
            mLiveAudienceViewHolder.clearAnim();
        }
        if (wsManager != null) {
            wsManager.stopConnect();
            wsManager = null;
        }
        if (mWebSocketClient != null) {
            mWebSocketClient.cancel();
            mWebSocketClient = null;
        }
        AppLog.e("LiveAudienceActivity-------onDestroy------->");
        super.onDestroy();
    }

    /**
     * 点亮
     */
    public void light() {
        if (!mLighted) {
            mLighted = true;
            int guardType = mLiveGuardInfo != null ? mLiveGuardInfo.getMyGuardType() : Constants.GUARD_TYPE_NONE;
            SocketChatUtil.sendLightMessage(mSocketClient, 1 + RandomUtil.nextInt(6), guardType);
        }
        if (mLiveRoomViewHolder != null) {
            mLiveRoomViewHolder.playLightAnim();
        }
    }


    /**
     * 计时收费更新主播映票数
     */
    public void roomChargeUpdateVotes() {
        sendUpdateVotesMessage(mLiveTypeVal);
    }

    /**
     * 暂停播放
     */
    public void pausePlay() {
        if (mLivePlayViewHolder != null) {
            mLivePlayViewHolder.pausePlay();
        }
    }

    /**
     * 恢复播放
     */
    public void resumePlay() {
        if (mLivePlayViewHolder != null) {
            mLivePlayViewHolder.resumePlay();
        }
    }

    /**
     * 充值成功
     */
    public void onChargeSuccess() {
        if (mLiveType == Constants.LIVE_TYPE_TIME) {
            if (mCoinNotEnough) {
                mCoinNotEnough = false;
                LiveHttpUtil.roomCharge(mLiveUid, mStream, new HttpCallback() {
                    @Override
                    public void onSuccess(int code, String msg, String[] info) {
                        if (code == 0) {
                            roomChargeUpdateVotes();
                            if (mLiveRoomViewHolder != null) {
                                resumePlay();
                                mLiveRoomViewHolder.startRequestTimeCharge();
                            }
                        } else {
                            if (code == 1008) {//余额不足
                                mCoinNotEnough = true;
                                DialogUitl.showSimpleDialog(mContext, WordUtil.getString(R.string.live_coin_not_enough), false,
                                        new DialogUitl.SimpleCallback2() {
                                            @Override
                                            public void onConfirmClick(Dialog dialog, String content) {
                                                RouteUtil.forwardMyCoin(mContext);
                                            }

                                            @Override
                                            public void onCancelClick() {
                                                exitLiveRoom();
                                            }
                                        });
                            }
                        }
                    }
                });
            }
        }
    }

    public void setCoinNotEnough(boolean coinNotEnough) {
        mCoinNotEnough = coinNotEnough;
    }

    /**
     * 游戏窗口变化事件
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGameWindowChangedEvent(GameWindowChangedEvent e) {
        if (mLiveRoomViewHolder != null) {
            mLiveRoomViewHolder.setOffsetY(e.getGameViewHeight());
        }
    }

    /**
     * 游戏充值页面
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOpenGameChargeEvent(OpenGameChargeEvent e) {
        openChargeWindow();
    }

    /**
     * 腾讯sdk连麦时候切换低延时流
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLinkMicTxAccEvent(LinkMicTxAccEvent e) {
        if (mLivePlayViewHolder != null && mLivePlayViewHolder instanceof LivePlayTxViewHolder) {
            mLivePlayViewHolder.onLinkMicTxAccEvent(e.isLinkMic());
        }
    }

    /**
     * 腾讯sdk时候主播连麦回调
     *
     * @param linkMic true开始连麦 false断开连麦
     */
    public void onLinkMicTxAnchor(boolean linkMic) {
        if (mLivePlayViewHolder != null && mLivePlayViewHolder instanceof LivePlayTxViewHolder) {
            mLivePlayViewHolder.setAnchorLinkMic(linkMic, 5000);
        }
    }


    /**
     * 打开充值窗口
     */
    public void openChargeWindow() {
        if (mPayPresenter == null) {
            mPayPresenter = new PayPresenter(this);
            mPayPresenter.setServiceNameAli(Constants.PAY_BUY_COIN_ALI);
            mPayPresenter.setServiceNameWx(Constants.PAY_BUY_COIN_WX);
            mPayPresenter.setAliCallbackUrl(HtmlConfig.ALI_PAY_COIN_URL);
            mPayPresenter.setPayCallback(new PayCallback() {
                @Override
                public void onSuccess() {
                    if (mPayPresenter != null) {
                        mPayPresenter.checkPayResult();
                    }
                }

                @Override
                public void onFailed() {

                }
            });
        }
        LiveChargeDialogFragment fragment = new LiveChargeDialogFragment();
        fragment.setLifeCycleListener(this);
        fragment.setPayPresenter(mPayPresenter);
        fragment.show(getSupportFragmentManager(), "ChatChargeDialogFragment");
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLiveRoomChangeEvent(LiveRoomChangeEvent e) {
        LiveBean liveBean = e.getLiveBean();
        if (liveBean != null) {
            LiveHttpUtil.cancel(LiveHttpConsts.CHECK_LIVE);
            LiveHttpUtil.cancel(LiveHttpConsts.ENTER_ROOM);
            LiveHttpUtil.cancel(LiveHttpConsts.ROOM_CHARGE);
            clearRoomData();

            setLiveRoomData(mainRecommendBean);
            mLiveType = e.getLiveType();
            mLiveTypeVal = e.getLiveTypeVal();
//            enterRoom();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLiveRoomInfoEvent(LiveRoomInfoEvent e) {
        if (mLiveChatViewHolder != null) {
            mLiveChatViewHolder.closeInfo();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveMathEvent(MatchEvent event) {
        int type = event.getType();
        AppLog.e("详情收到赛事通知：" + type);
        switch (type) {
            case 1:
                try {
                    ArrayList<GameLolMatchBean> lolBeanList = event.getLolBeanList();
                    if (lolBeanList != null && !lolBeanList.isEmpty()) {
                        AppLog.e("详情收到赛事通知：刷新数据");
                        for (GameLolMatchBean gameLolMatchBean : lolBeanList) {
                            if (mMatchInfo.getMatch_id().equals(gameLolMatchBean.getMatch_id())) {
                                if (mLiveGameDataViewHolder != null)
                                    mLiveGameDataViewHolder.loadData(mMatchInfo, 1);
                                if (mLiveExponentViewHolder != null)
                                    mLiveExponentViewHolder.loadData(mMatchInfo);
                            }
                        }
                    }
                } catch (Exception e) {
                    AppLog.e(e.toString());
                }
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }


    /**
     * 打开商品窗口
     */
    public void openGoodsWindow() {
        LiveGoodsDialogFragment fragment = new LiveGoodsDialogFragment();
        fragment.setLifeCycleListener(this);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.LIVE_UID, mLiveUid);
        fragment.setArguments(bundle);
        fragment.show(getSupportFragmentManager(), "LiveGoodsDialogFragment");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                showFullScreen();
                return true;
            }
            if (mMatchInfo != null && mLiveLlMatchInfo != null) {
                if (mLivePlayViewHolder != null) {
                    mLivePlayViewHolder.stopPlay();
                }
                if (mLiveLlMatchInfo.getVisibility() == View.GONE) {
                    mLiveLlMatchInfo.setVisibility(View.VISIBLE);
                } else {
                    return super.onKeyDown(keyCode, event);
                }

                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 离开直播间
     */
    public void leaveRoom() {
        if (mainRecommendBean != null) {
            String stream = mainRecommendBean.getStream();
            LiveHttpUtil.leaveRoom(stream, new HttpCallback() {
                @Override
                public void onSuccess(int code, String msg, String[] info) {

                }
            });
        }
    }

}
