package com.yunbao.live.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.fastjson.JSON;
import com.gyf.immersionbar.ImmersionBar;
import com.yunbao.common.Constants;
import com.yunbao.common.adapter.ViewPagerAdapter;
import com.yunbao.common.bean.GameBasketballMatchBean;
import com.yunbao.common.bean.GameFootballMatchBean;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.http.HttpCallbackObject;
import com.yunbao.common.utils.DateFormatUtil;
import com.yunbao.common.utils.DpUtil;
import com.yunbao.common.utils.EasyAES;
import com.yunbao.common.utils.MD5Util;
import com.yunbao.common.views.AbsViewHolder;
import com.yunbao.live.R;
import com.yunbao.live.event.MatchEvent;
import com.yunbao.live.http.LiveHttpUtil;
import com.yunbao.live.views.LiveGameChatViewHolder;
import com.yunbao.live.views.LivePlayTxViewHolder;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class LiveSportsActivity extends LiveActivity implements View.OnClickListener {

    private TextView title, tvDateInfo, tvTeamOne, tvTeamTwo, tvLiveState, tvHalf, tvScoreOne, tvScoreTwo, tvStatus, tvSeconds;
    private ImageView ivTeamOne, ivTeamTwo;
    private ArrayList<FrameLayout> mViewList;
    private ViewPager mViewPager;
    private MagicIndicator mIndicator;
    private String[] mBottomTitles = new String[]{"主播"};
    private AbsViewHolder[] mAbsViewHolders;
    private LiveGameChatViewHolder mLiveChatViewHolder;
    private GameFootballMatchBean mFootballInfo;
    private GameBasketballMatchBean mBasketBallInfo;
    private String id;
    private LinearLayout llBg;
    private LivePlayTxViewHolder mLivePlayViewHolder;
    private View tvLive;
    private View playContainer;
    private int matchType;
    public View mRlTitle;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_live_sports;
    }

    @Override
    protected void getInentParams() {
        super.getInentParams();
        Intent intent = getIntent();
        mFootballInfo = (GameFootballMatchBean) intent.getSerializableExtra(Constants.LIVE_MATCH_FOOTBALL_INFO);
        mBasketBallInfo = (GameBasketballMatchBean) intent.getSerializableExtra(Constants.LIVE_MATCH_BASKETBALL_INFO);
        id = intent.getStringExtra("id");
        matchType = intent.getIntExtra("match_type", 3);
    }

    public void backClick(View v) {
        if (v.getId() == com.yunbao.common.R.id.btn_back) {
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                finish();
                return;
            }
            showFullScreen();
        }
    }

    @Override
    protected void main() {
        super.main();
        ImmersionBar.with(this).statusBarDarkFont(false).navigationBarColor(R.color.white).navigationBarDarkIcon(true).init();
        title = findViewById(R.id.tv_title);
        mRlTitle = findViewById(R.id.ll_game_title);
        tvDateInfo = findViewById(R.id.live_tv_date_info);
        ivTeamOne = findViewById(R.id.live_iv_team_one_icon);
        ivTeamTwo = findViewById(R.id.live_iv_team_two_icon);
        tvTeamOne = findViewById(R.id.live_iv_team_one_name);
        tvTeamTwo = findViewById(R.id.live_iv_team_two_name);
        tvScoreOne = findViewById(R.id.live_tv_score_one);
        tvScoreTwo = findViewById(R.id.live_tv_score_two);
        tvLiveState = findViewById(R.id.live_view_stats);
        tvSeconds = findViewById(R.id.live_tv_seconds);
        tvStatus = findViewById(R.id.live_tv_txt_status);
        llBg = findViewById(R.id.live_ll_match_info);
        mViewPager = findViewById(R.id.viewPager);
        mIndicator = findViewById(R.id.indicator);
        tvHalf = findViewById(R.id.tv_half);
        tvLive = findViewById(R.id.live_tv_living);
        playContainer = findViewById(R.id.play_container);
        mAbsViewHolders = new AbsViewHolder[mBottomTitles.length];
        mLivePlayViewHolder = new LivePlayTxViewHolder(mContext, (ViewGroup) playContainer);
        mLivePlayViewHolder.addToParent();
        mLivePlayViewHolder.subscribeActivityLifeCycle();
        tvLive.setOnClickListener(this);
        setData();
        getData();
        setViewPage();
        loadPageData(0);
    }

    private void getData() {
        if (matchType == 2) {
            getBasketBallInfo();
        }
        if (matchType == 3) {
            getFootBallInfo();
        }
    }

    @Subscribe
    public void onReceiveMathEvent(MatchEvent event) {
        int type = event.getType();
        switch (type) {
            case 1:
                break;
            case 2:
                setBaseKetBallInfo(event.getBasketBallBeanList());
                break;
            case 3:
                setFootBallInfo(event.getFootballBallBeanList());
                break;
        }
    }

    private void setFootBallInfo(ArrayList<GameFootballMatchBean> footballBallBeanList) {
        if (footballBallBeanList == null) return;
        for (GameFootballMatchBean bean : footballBallBeanList) {
            if (bean.getMatchId().equals(id)) {
                int isPlaying = bean.isIs_playing();
                String state = bean.getState_str();
                if (isPlaying == 1) {
                    tvLiveState.setText(R.string.vs);
                    tvStatus.setText(state);
                } else if (isPlaying == 2) {
                    tvScoreOne.setText(mFootballInfo.getHomeScore());
                    tvScoreTwo.setText(mFootballInfo.getAwayScore());
                    tvHalf.setText(state);
                } else {
                    tvScoreOne.setText(mFootballInfo.getHomeScore());
                    tvScoreTwo.setText(mFootballInfo.getAwayScore());
                    tvStatus.setText(state);
                }
            }
        }
    }

    private void setBaseKetBallInfo(ArrayList<GameBasketballMatchBean> basketBallBeanList) {
        if (basketBallBeanList == null) return;
        for (GameBasketballMatchBean bean : basketBallBeanList) {
            if (bean.getMatchId().equals(id)) {
                int isPlaying = bean.isIs_playing();
                String state = bean.getState_str();
                if (isPlaying == 1) {
                    tvLiveState.setText(R.string.vs);
                    tvStatus.setText(state);
                } else if (isPlaying == 2) {
                    tvScoreOne.setText(mFootballInfo.getHomeScore());
                    tvScoreTwo.setText(mFootballInfo.getAwayScore());
                    tvHalf.setText(state);
                } else {
                    tvScoreOne.setText(mFootballInfo.getHomeScore());
                    tvScoreTwo.setText(mFootballInfo.getAwayScore());
                    tvStatus.setText(state);
                }
            }
        }
    }


    private void getBasketBallInfo() {
        if (mBasketBallInfo == null) {
            LiveHttpUtil.getBasketBallMatch(id, new HttpCallbackObject() {
                @Override
                public void onSuccess(int code, String msg, Object info) {
                    if (info != null) {
                        mBasketBallInfo = JSON.parseObject(info.toString(), GameBasketballMatchBean.class);
                        setData();
                    }
                }
            });
        }
    }

    private void getFootBallInfo() {
        if (mFootballInfo == null) {
            LiveHttpUtil.getFooBallMatch(id, new HttpCallbackObject() {
                @Override
                public void onSuccess(int code, String msg, Object info) {
                    if (info != null) {
                        mFootballInfo = JSON.parseObject(info.toString(), GameFootballMatchBean.class);
                        setData();
                    }
                }
            });
        }
    }


    @SuppressLint("SetTextI18n")
    private void setData() {
        //比赛状态:-14:推迟，-13:中断，-12:腰斩，-11:待定，-10:取消，-1.完场，0:未开始，1:上半场，2:中场，3:下半场，4:加时，5:点球
        if (mFootballInfo != null) {
            GameFootballMatchBean.HomeTeamBean homeTeam = mFootballInfo.getHome_team();
            GameFootballMatchBean.AwayTeamBean awayTeam = mFootballInfo.getAway_team();
            if (homeTeam != null) {
                ImgLoader.displayWithError(this, homeTeam.getLogo(), ivTeamOne, R.mipmap.icon_live_big_crown);
                tvTeamOne.setText(homeTeam.getNameCn());
            }
            if (awayTeam != null) {
                ImgLoader.displayWithError(this, mFootballInfo.getAway_team().getLogo(), ivTeamTwo, R.mipmap.icon_live_big_crown);
                tvTeamTwo.setText(awayTeam.getNameCn());
            }

            title.setText(mFootballInfo.getLeague() == null ? "" : mFootballInfo.getLeague().getLeagueNameCnShort());
            String time = DateFormatUtil.getMatchTime(Long.parseLong(mFootballInfo.getMatchStartTime()) * 1000L, "yyyy-MM-dd HH:mm");
            tvDateInfo.setText(time);
            llBg.setBackgroundResource(R.mipmap.bg_default_live_football);
            //1未开赛，2比赛中，3已完赛
            int isPlaying = mFootballInfo.isIs_playing();
            if (isPlaying == 1) {
                tvLiveState.setText(R.string.vs);
                tvStatus.setText(mFootballInfo.getState_str());
                tvLive.setVisibility(View.GONE);
            } else if (isPlaying == 2) {
                tvScoreOne.setText(mFootballInfo.getHomeScore());
                tvScoreTwo.setText(mFootballInfo.getAwayScore());
                tvHalf.setText(mFootballInfo.getState_str());
                List<String> liveUrl = mFootballInfo.getLive_url();
                if (liveUrl.isEmpty()) {
                    tvLive.setVisibility(View.GONE);
                } else {
                    tvLive.setVisibility(View.VISIBLE);
                }
            } else {
                tvScoreOne.setText(mFootballInfo.getHomeScore());
                tvScoreTwo.setText(mFootballInfo.getAwayScore());
                tvStatus.setText(mFootballInfo.getState_str());
                tvLive.setVisibility(View.GONE);
            }
        }

        if (mBasketBallInfo != null) {
            GameBasketballMatchBean.HomeTeamBean homeTeam = mBasketBallInfo.getHome_team();
            GameBasketballMatchBean.AwayTeamBean awayTeam = mBasketBallInfo.getAway_team();
            if (homeTeam != null) {
                ImgLoader.display(this, homeTeam.getLogo(), ivTeamOne);
                tvTeamOne.setText(homeTeam.getNameCn());
            }
            if (awayTeam != null) {
                ImgLoader.display(this, awayTeam.getLogo(), ivTeamTwo);
                tvTeamTwo.setText(awayTeam.getNameCn());
            }
            int matchConductTime = mBasketBallInfo.getMatchConductTime();
            if (mBasketBallInfo.getMatchConductTime() != 0) {
                tvSeconds.setText(mBasketBallInfo.getState_str() + (matchConductTime / 60) + ":" + (matchConductTime % 60));
            } else {
                tvSeconds.setVisibility(View.GONE);
            }
            title.setText(mBasketBallInfo.getLeague() == null ? "" : mBasketBallInfo.getLeague().getLeagueNameCnShort());
            String time = DateFormatUtil.getMatchTime(Long.parseLong(mBasketBallInfo.getMatchStartTime()) * 1000L, "yyyy-MM-dd HH:mm");
            tvDateInfo.setText(time);
            llBg.setBackgroundResource(R.mipmap.bg_default_live_basketball);
            //1未开赛，2比赛中，3已完赛
            int isPlaying = mBasketBallInfo.isIs_playing();
            if (isPlaying == 1) {
                tvLiveState.setText(R.string.vs);
                tvStatus.setText(mBasketBallInfo.getState_str());
            } else if (isPlaying == 2) {
                tvScoreOne.setText(mBasketBallInfo.getHomeScore());
                tvScoreTwo.setText(mBasketBallInfo.getAwayScore());
                tvHalf.setText(mBasketBallInfo.getState_str());
                List<String> liveUrl = mBasketBallInfo.getLiveUrl();
                if (liveUrl.isEmpty()) {
                    tvLive.setVisibility(View.GONE);
                } else {
                    tvLive.setVisibility(View.VISIBLE);
                }
            } else {
                tvScoreOne.setText(mBasketBallInfo.getHomeScore());
                tvScoreTwo.setText(mBasketBallInfo.getAwayScore());
            }

        }
    }

    private void setViewPage() {
        mViewList = new ArrayList<>();
        for (int i = 0; i < 1; i++) {
            FrameLayout frameLayout = new FrameLayout(mContext);
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            mViewList.add(frameLayout);
        }

        mViewPager.setAdapter(new ViewPagerAdapter(mViewList));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                loadPageData(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mIndicator = findViewById(R.id.indicator);
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {

            @Override
            public int getCount() {
                return mBottomTitles.length;
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                return getIndicatorTitleView(context, mBottomTitles, index);
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_MATCH_EDGE);
                linePagerIndicator.setXOffset(DpUtil.dp2px(5));
                linePagerIndicator.setRoundRadius(DpUtil.dp2px(2));
                linePagerIndicator.setColors(ContextCompat.getColor(mContext, R.color.live_ff5116));
                return null;
            }
        });
        mIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mIndicator, mViewPager);
    }

    protected IPagerTitleView getIndicatorTitleView(Context context, String[] titles, final int index) {
        SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
        simplePagerTitleView.setNormalColor(ContextCompat.getColor(mContext, R.color.live_2B2626));
        simplePagerTitleView.setSelectedColor(ContextCompat.getColor(mContext, R.color.live_ff5116));
        simplePagerTitleView.setText(titles[index]);
        simplePagerTitleView.setTextSize(13);
        simplePagerTitleView.getPaint().setFakeBoldText(true);
        simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager != null) {
                    mViewPager.setCurrentItem(index);
                }
            }
        });
        return simplePagerTitleView;
    }

    private void loadPageData(int position) {
        if (mAbsViewHolders == null) {
            return;
        }
        AbsViewHolder vh = mAbsViewHolders[position];
        if (vh == null) {
            if (mViewList != null && position < mViewList.size()) {
                FrameLayout parent = mViewList.get(position);
                if (parent == null) {
                    return;
                }
                if (position == 0) {
                    mLiveChatViewHolder = new LiveGameChatViewHolder(mContext, parent, mFootballInfo != null ? LiveGameChatViewHolder.TYPE_FOOTBALL : LiveGameChatViewHolder.TYPE_BASKETBALL);
                    vh = mLiveChatViewHolder;
                }

                mAbsViewHolders[position] = vh;
                vh.addToParent();
                vh.subscribeActivityLifeCycle();
            }
        }
        if (vh instanceof LiveGameChatViewHolder) {
            mLiveChatViewHolder.isMatch();
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.live_tv_living) {
            playContainer.setVisibility(View.VISIBLE);
            if (mFootballInfo != null) {
                List<String> liveUrl = mFootballInfo.getLive_url();
                playLive(liveUrl, mFootballInfo.getMatchId());
            } else {
                List<String> liveUrl = mBasketBallInfo.getLiveUrl();
                playLive(liveUrl, mBasketBallInfo.getMatchId());
            }
        }
    }

    //播放直播
    private void playLive(List<String> liveUrl, String matchId) {
        mLivePlayViewHolder.setPlayMode("1", "1");
        mLivePlayViewHolder.setLiveId(matchId);
        String url = liveUrl.get(0);
        String mUrl = EasyAES.decryptString(url);
        String stream = Uri.parse(mUrl).getLastPathSegment().split("\\.")[0];
        String resultUrl = mUrl + "?txSecret=" + MD5Util.getMD5("02019d98d6084be641cbf67ce3aa65fc"
                + stream + Long.toHexString(System.currentTimeMillis() / 1000))
                + "&txTime=" + Long.toHexString(System.currentTimeMillis() / 1000);
        mLivePlayViewHolder.play(resultUrl);
    }

    public void showFullScreen() {
        setConfigDisplay(playContainer, false);
        mLivePlayViewHolder.changeWaterMark();
    }


}
