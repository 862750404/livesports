package com.yunbao.main.bean;

public class AnchorAnchorBean {


    /**
     * id : 7
     * user_nicename : 手机用户3227
     * avatar : /default.jpg
     * avatar_thumb : /default_thumb.jpg
     * isSubscribe : 0
     */

    private String id;
    private String user_nicename;
    private String avatar;
    private String avatar_thumb;
    private int isSubscribe;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_nicename() {
        return user_nicename;
    }

    public void setUser_nicename(String user_nicename) {
        this.user_nicename = user_nicename;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar_thumb() {
        return avatar_thumb;
    }

    public void setAvatar_thumb(String avatar_thumb) {
        this.avatar_thumb = avatar_thumb;
    }

    public int getIsSubscribe() {
        return isSubscribe;
    }

    public void setIsSubscribe(int isSubscribe) {
        this.isSubscribe = isSubscribe;
    }
}
