package com.yunbao.live.bean;

import java.util.List;

/**
 * @author Juwan
 * @date 2020/10/2
 * Description:
 */
public class LiveGamePlayerBean {

    /**
     * battle_current_id : 6688701
     * team_a_name : LGC
     * match_id : 66887
     * league_name : S10 世界总决赛
     * team_a_logo : https://qn.feijing88.com/feijing-home/egame/image/2020917/864196c825504efc8bb97fc7dfc10a14.png
     * starttime : 19:00
     * team_b_name : SUP
     * team_b_logo : https://qn.feijing88.com/feijing-home/egame/image/2020917/824a28139cd34f9ca5e7b4da0fe327d9.png
     * startdate : 09-28
     * team_a_score : 1
     * start_time : 2020-09-28
     * team_b_score : 0
     * viewnum : 2077
     * team_b_player_stats : [{"game_count":9,"player_id":"850","damage_per_min":174.836,"kda":5.25,"golds_per_min":255.126,"player_name":"SnowFlower","team_id":"85","position":"辅助","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190622/d758accd60c349fdb0d501684a701d0d.png","hero_pick_stats":[{"hero_name_en":"Pantheon","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/2020324/34f2d694132749aea3f52453b059d5c0.png","hero_name":"不屈之枪","hero_game_count":2,"hero_win_count":2,"hero_id":80},{"hero_name_en":"Tahm Kench","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190621/052d2c25823946438b9e6f342e26d9d5.png","hero_name":"河流之王","hero_game_count":1,"hero_win_count":1,"hero_id":102},{"hero_name_en":"Morgana","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20191015/31a812a21aad404c8a13f5d3bd6ae42b.png","hero_name":"堕落天使","hero_game_count":1,"hero_win_count":1,"hero_id":71},{"hero_name_en":"Sett","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/2020827/a1f031d6ca664cb4ba942d7261b19b96.png","hero_name":"腕豪","hero_game_count":3,"hero_win_count":2,"hero_id":26807153},{"hero_name_en":"Nautilus","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/418257da890a4fcfa8af2c64920530a1.jpg","hero_name":"深海泰坦","hero_game_count":1,"hero_win_count":0,"hero_id":74},{"hero_name_en":"Thresh","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/6ea3191f2ed34c93bb7fb728b25ca787.jpg","hero_name":"魂锁典狱长","hero_game_count":1,"hero_win_count":1,"hero_id":107}],"win_rate":0.778,"offered_rate":0.717},{"game_count":9,"player_id":"26635088192","damage_per_min":311.51,"kda":4,"golds_per_min":352.731,"player_name":"Armut","team_id":"85","position":"上单","avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","hero_pick_stats":[{"hero_name_en":"Ornn","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20191111/a04d59c30e8046acaade95cca180e293.png","hero_name":"山隐之焰","hero_game_count":6,"hero_win_count":4,"hero_id":208},{"hero_name_en":"Kennen","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/23369b0f75774c82b309f668d84fb136.jpg","hero_name":"狂暴之心","hero_game_count":1,"hero_win_count":1,"hero_id":52},{"hero_name_en":"Wukong","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/0a6ae71a384c44e98815c0fb40135579.png","hero_name":"齐天大圣","hero_game_count":1,"hero_win_count":1,"hero_id":69},{"hero_name_en":"Shen","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/4871b14f12744d6aa96c041a685a969a.jpg","hero_name":"暮光之眼","hero_game_count":1,"hero_win_count":1,"hero_id":92}],"win_rate":0.778,"offered_rate":0.6},{"game_count":9,"player_id":"389","damage_per_min":280.042,"kda":4.267,"golds_per_min":368.608,"player_name":"KaKAO","team_id":"85","position":"打野","avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","hero_pick_stats":[{"hero_name_en":"Graves","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/f4704965744f4ed2adda19384fffb005.jpg","hero_name":"法外狂徒","hero_game_count":3,"hero_win_count":2,"hero_id":35},{"hero_name_en":"Hecarim","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/7f210483106d4dd0976284c233b92b10.jpg","hero_name":"战争之影","hero_game_count":1,"hero_win_count":1,"hero_id":36},{"hero_name_en":"Kindred","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/ad4a7484563d489e982c309f45fd9e6f.png","hero_name":"永猎双子","hero_game_count":1,"hero_win_count":1,"hero_id":54},{"hero_name_en":"Nidalee","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e90ed3f198064e9190426cf95986b02d.png","hero_name":"狂野女猎手","hero_game_count":2,"hero_win_count":1,"hero_id":75},{"hero_name_en":"Trundle","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/792e81b2080b4cd887c8c66e2fb408b9.png","hero_name":"巨魔之王","hero_game_count":1,"hero_win_count":1,"hero_id":109},{"hero_name_en":"Olaf","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/559f0cf75a514688863e8bf6c62c8712.png","hero_name":"狂战士","hero_game_count":1,"hero_win_count":1,"hero_id":78}],"win_rate":0.778,"offered_rate":0.683},{"game_count":9,"player_id":"971","damage_per_min":441.059,"kda":6.111,"golds_per_min":447.031,"player_name":"Zeitnot","team_id":"85","position":"ADC","avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","hero_pick_stats":[{"hero_name_en":"Kalista","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/da79d88a9db34287b26630074c83af03.png","hero_name":"复仇之矛","hero_game_count":3,"hero_win_count":3,"hero_id":46},{"hero_name_en":"Caitlyn","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/1f81145ea3a549759575d64a7d8e665c.jpg","hero_name":"皮城女警","hero_game_count":5,"hero_win_count":3,"hero_id":15},{"hero_name_en":"Aphelios","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/2020113/b769e29fcbc64d338deae625818447c3.png","hero_name":"残月之肃","hero_game_count":1,"hero_win_count":1,"hero_id":26807158}],"win_rate":0.778,"offered_rate":0.683},{"game_count":9,"player_id":"7309","damage_per_min":566.175,"kda":4.214,"golds_per_min":391.514,"player_name":"Bolulu","team_id":"85","position":"中单","avatar":"https://qn.feijing88.com/feijing-home/egame/image/lol/player.png","hero_pick_stats":[{"hero_name_en":"Zoe","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/a77a4745ff474125b2d3b2d262792000.png","hero_name":"暮光星灵","hero_game_count":4,"hero_win_count":4,"hero_id":211},{"hero_name_en":"Vel'Koz","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/b5c6d78c037e401aa09d339ce9b64134.jpg","hero_name":"虚空之眼","hero_game_count":1,"hero_win_count":1,"hero_id":118},{"hero_name_en":"Azir","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png","hero_name":"沙漠皇帝","hero_game_count":1,"hero_win_count":1,"hero_id":10},{"hero_name_en":"Lucian","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/ed438def9f8d41cca744bfef283d5942.jpg","hero_name":"圣枪游侠","hero_game_count":1,"hero_win_count":0,"hero_id":61},{"hero_name_en":"Galio","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/c51f6a6aaa5e4c95a7397df3f93a364e.jpg","hero_name":"正义巨像","hero_game_count":2,"hero_win_count":1,"hero_id":30}],"win_rate":0.778,"offered_rate":0.708}]
     * league_cat : 英雄联盟
     * round_son_name : 第四日
     * team_a_player_stats : [{"game_count":10,"damage_per_min":471.512,"kda":2.941,"golds_per_min":402.095,"player_name":"Topoon","team_id":"877","position":"上单","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190924/42c4c787965a4a029eedbba99ccebc54.png","hero_pick_stats":[{"hero_name_en":"Ornn","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20191111/a04d59c30e8046acaade95cca180e293.png","hero_name":"山隐之焰","hero_game_count":1,"hero_win_count":1,"hero_id":208},{"hero_name_en":"Gnar","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e8e3567660164010ba60f75c00e6fed6.png","hero_name":"迷失之牙","hero_game_count":2,"hero_win_count":1,"hero_id":33},{"hero_name_en":"Cho'Gath","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/39f8c5c3750a4ff7ac37d47eeab868a2.png","hero_name":"虚空恐惧","hero_game_count":1,"hero_win_count":0,"hero_id":17},{"hero_name_en":"Aatrox","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190621/355dbeb592aa46b5ac522cebfcf52aeb.png","hero_name":"暗裔剑魔","hero_game_count":1,"hero_win_count":0,"hero_id":1},{"hero_name_en":"Renekton","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/180c5a712c2242259fe9ef7b17c35a43.jpg","hero_name":"荒漠屠夫","hero_game_count":3,"hero_win_count":3,"hero_id":85},{"hero_name_en":"Camille","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/d3d8145e2cf948aa967aa9a11429396a.jpg","hero_name":"青钢影","hero_game_count":1,"hero_win_count":1,"hero_id":134},{"hero_name_en":"Gangplank","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/e15ccebb865e4b65af2d90b2df1ffc69.jpg","hero_name":"海洋之灾","hero_game_count":1,"hero_win_count":1,"hero_id":31}],"win_rate":0.7,"offered_rate":0.553},{"game_count":10,"damage_per_min":202.847,"kda":13.286,"golds_per_min":242.12,"player_name":"Isles","team_id":"877","position":"辅助","avatar":"https://qn.feijing88.com/feijing-home/egame/image/2020923/58ea9f5d64bc4830bb7bb51e57f5f76c.png","hero_pick_stats":[{"hero_name_en":"Morgana","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20191015/31a812a21aad404c8a13f5d3bd6ae42b.png","hero_name":"堕落天使","hero_game_count":1,"hero_win_count":1,"hero_id":71},{"hero_name_en":"Bard","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/203e0855a5134732a79d27f19443055d.png","hero_name":"星界游神","hero_game_count":3,"hero_win_count":1,"hero_id":11},{"hero_name_en":"Blitzcrank","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20191015/38ee05a7738a490f887f5fa48925b288.png","hero_name":"蒸汽机器人","hero_game_count":1,"hero_win_count":1,"hero_id":12},{"hero_name_en":"Lulu","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/5e7c442dc2d24009b1b1a93a15f16ca9.jpg","hero_name":"仙灵女巫","hero_game_count":1,"hero_win_count":1,"hero_id":62},{"hero_name_en":"Lux","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190621/627f4838849343d5858faa1fcdfdd5fa.png","hero_name":"光辉女郎","hero_game_count":4,"hero_win_count":3,"hero_id":63}],"win_rate":0.7,"offered_rate":0.712},{"game_count":4,"damage_per_min":394.219,"kda":1.3,"golds_per_min":377.763,"player_name":"LGC Halo","team_id":"877","position":"中单","avatar":"https://qn.feijing88.com/feijing-home/egame/image/2020923/192733fe9fdd4126a762cd3f130f5213.png","hero_pick_stats":[{"hero_name_en":"Azir","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ff1077ad25aa4dee871e55a11395faba.png","hero_name":"沙漠皇帝","hero_game_count":1,"hero_win_count":0,"hero_id":10},{"hero_name_en":"Galio","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/c51f6a6aaa5e4c95a7397df3f93a364e.jpg","hero_name":"正义巨像","hero_game_count":2,"hero_win_count":1,"hero_id":30},{"hero_name_en":"Orianna","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/669929b6c71a4e2da66c96994ce574e1.jpg","hero_name":"发条魔灵","hero_game_count":1,"hero_win_count":1,"hero_id":79}],"win_rate":0.5,"offered_rate":0.174},{"game_count":10,"damage_per_min":262.08,"kda":5,"golds_per_min":347.684,"player_name":"Babip","team_id":"877","position":"打野","avatar":"https://qn.feijing88.com/feijing-home/egame/image/2020923/051a417d5eaf4648b97333ee5818cff7.png","hero_pick_stats":[{"hero_name_en":"Graves","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/f4704965744f4ed2adda19384fffb005.jpg","hero_name":"法外狂徒","hero_game_count":1,"hero_win_count":0,"hero_id":35},{"hero_name_en":"Jarvan IV","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/d219824892304f558c01d6bf2fc01b69.jpg","hero_name":"德玛西亚皇子","hero_game_count":1,"hero_win_count":0,"hero_id":41},{"hero_name_en":"Sejuani","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190621/e7daa2f05a924ebcb6ce6bbb023402d3.png","hero_name":"凛冬之怒","hero_game_count":1,"hero_win_count":1,"hero_id":90},{"hero_name_en":"Volibear","hero_logo":"https://qn.feijing88.com/egame/lol/hero/30ebb0d30b26204beba8257825ffc9e3.png","hero_name":"不灭狂雷","hero_game_count":1,"hero_win_count":0,"hero_id":122},{"hero_name_en":"Nidalee","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e90ed3f198064e9190426cf95986b02d.png","hero_name":"狂野女猎手","hero_game_count":2,"hero_win_count":2,"hero_id":75},{"hero_name_en":"Trundle","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/792e81b2080b4cd887c8c66e2fb408b9.png","hero_name":"巨魔之王","hero_game_count":2,"hero_win_count":2,"hero_id":109},{"hero_name_en":"Olaf","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/559f0cf75a514688863e8bf6c62c8712.png","hero_name":"狂战士","hero_game_count":2,"hero_win_count":2,"hero_id":78}],"win_rate":0.7,"offered_rate":0.644},{"game_count":6,"damage_per_min":449.42,"kda":6.833,"golds_per_min":388.514,"player_name":"Tally","team_id":"877","position":"上单","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190924/72cd8561ca6d4144a796d8452864de79.png","hero_pick_stats":[{"hero_name_en":"Kassadin","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/aadb962ffb6f468294f52afe68ef6dc5.jpg","hero_name":"虚空行者","hero_game_count":1,"hero_win_count":1,"hero_id":49},{"hero_name_en":"Ziggs","hero_logo":"https://qn.feijing88.com/egame/lol/hero/b5dd866994de7d6f24601107da5c886a.png","hero_name":"爆破鬼才","hero_game_count":2,"hero_win_count":2,"hero_id":130},{"hero_name_en":"Galio","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/c51f6a6aaa5e4c95a7397df3f93a364e.jpg","hero_name":"正义巨像","hero_game_count":2,"hero_win_count":1,"hero_id":30},{"hero_name_en":"Karma","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/6ec98ab18fcf4bafb93c9835d0a5beb9.jpg","hero_name":"天启者","hero_game_count":1,"hero_win_count":1,"hero_id":47}],"win_rate":0.833,"offered_rate":0.364},{"game_count":10,"damage_per_min":603.245,"kda":2.824,"golds_per_min":458.018,"player_name":"Raes","team_id":"877","position":"ADC","avatar":"https://qn.feijing88.com/feijing-home/egame/image/20190924/24bb7be1439944bb910496b8333ea494.png","hero_pick_stats":[{"hero_name_en":"Ziggs","hero_logo":"https://qn.feijing88.com/egame/lol/hero/b5dd866994de7d6f24601107da5c886a.png","hero_name":"爆破鬼才","hero_game_count":1,"hero_win_count":1,"hero_id":130},{"hero_name_en":"Senna","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20191228/69428d235950432d8853b63e6f739370.png","hero_name":"涤魂圣枪","hero_game_count":2,"hero_win_count":1,"hero_id":26807148},{"hero_name_en":"Ashe","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190621/ede85ab907584db497cdeebd4be64033.png","hero_name":"寒冰射手","hero_game_count":1,"hero_win_count":0,"hero_id":8},{"hero_name_en":"Ezreal","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/6e26586655de48608aeec7c88a31067e.jpg","hero_name":"探险家","hero_game_count":1,"hero_win_count":1,"hero_id":26},{"hero_name_en":"Jhin","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/3226440a6b124d2aa624a409cfa4b6b7.jpg","hero_name":"戏命师","hero_game_count":1,"hero_win_count":1,"hero_id":44},{"hero_name_en":"Caitlyn","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/1f81145ea3a549759575d64a7d8e665c.jpg","hero_name":"皮城女警","hero_game_count":4,"hero_win_count":3,"hero_id":15}],"win_rate":0.7,"offered_rate":0.674}]
     * battle_list : [{"battle_id":6688701,"match_id":66887,"duration":1780,"index":1,"economic_diff":[{"time":0,"diff":0},{"time":60,"diff":0},{"time":120,"diff":11},{"time":180,"diff":-216},{"time":240,"diff":-594},{"time":300,"diff":-632},{"time":360,"diff":-555},{"time":420,"diff":-757},{"time":480,"diff":-626},{"time":540,"diff":-536},{"time":600,"diff":3},{"time":660,"diff":-206},{"time":720,"diff":400},{"time":780,"diff":1019},{"time":840,"diff":1057},{"time":900,"diff":1202},{"time":960,"diff":1128},{"time":1020,"diff":1243},{"time":1080,"diff":1631},{"time":1140,"diff":1742},{"time":1200,"diff":1523},{"time":1260,"diff":2821},{"time":1320,"diff":5557},{"time":1380,"diff":5289},{"time":1440,"diff":7987},{"time":1500,"diff":8465},{"time":1560,"diff":9525},{"time":1620,"diff":8913},{"time":1680,"diff":8549},{"time":1740,"diff":9152}],"xp_diff":[],"status":1,"start_time":0}]
     * battle_current_index : 1
     * id : 1
     * league_id : 634
     * status : 2
     * battle_duration : 1780
     */

    private int battle_current_id; //当前对局id
    private String team_a_name;  //	主队名
    private String match_id;  //比赛ID
    private String league_name;  //联赛名称
    private String team_a_logo;  //主队LOGO
    private String starttime;  //比赛开始时间
    private String team_b_name; //客队名
    private String team_b_logo; //客队LOGO
    private String startdate;  //比赛开始日期
    private String team_a_score;   //主队得分
    private String start_time;     //完整的开始日期
    private String team_b_score;  //客队得分
    private int viewnum;    //观看次数
    private String league_cat;  //联赛分类
    private String round_son_name;  //子轮次名次
    private String battle_list;
    private int battle_current_index;  //当前对局局数
    private String id;  //id
    private String league_id; //联赛ID
    private String status; //比赛状态 0:未开始 1:进行中 2:已结束 3:已延期 4:已删除
    private int battle_duration;  //当前对局时长
    private List<TeamPlayerStatsBean> team_b_player_stats;  //B队选手对位数据
    private List<TeamPlayerStatsBean> team_a_player_stats;  //	A队选手对位数据

    public int getBattle_current_id() {
        return battle_current_id;
    }

    public void setBattle_current_id(int battle_current_id) {
        this.battle_current_id = battle_current_id;
    }

    public String getTeam_a_name() {
        return team_a_name;
    }

    public void setTeam_a_name(String team_a_name) {
        this.team_a_name = team_a_name;
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getLeague_name() {
        return league_name;
    }

    public void setLeague_name(String league_name) {
        this.league_name = league_name;
    }

    public String getTeam_a_logo() {
        return team_a_logo;
    }

    public void setTeam_a_logo(String team_a_logo) {
        this.team_a_logo = team_a_logo;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getTeam_b_name() {
        return team_b_name;
    }

    public void setTeam_b_name(String team_b_name) {
        this.team_b_name = team_b_name;
    }

    public String getTeam_b_logo() {
        return team_b_logo;
    }

    public void setTeam_b_logo(String team_b_logo) {
        this.team_b_logo = team_b_logo;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getTeam_a_score() {
        return team_a_score;
    }

    public void setTeam_a_score(String team_a_score) {
        this.team_a_score = team_a_score;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getTeam_b_score() {
        return team_b_score;
    }

    public void setTeam_b_score(String team_b_score) {
        this.team_b_score = team_b_score;
    }

    public int getViewnum() {
        return viewnum;
    }

    public void setViewnum(int viewnum) {
        this.viewnum = viewnum;
    }

    public String getLeague_cat() {
        return league_cat;
    }

    public void setLeague_cat(String league_cat) {
        this.league_cat = league_cat;
    }

    public String getRound_son_name() {
        return round_son_name;
    }

    public void setRound_son_name(String round_son_name) {
        this.round_son_name = round_son_name;
    }

    public String getBattle_list() {
        return battle_list;
    }

    public void setBattle_list(String battle_list) {
        this.battle_list = battle_list;
    }

    public int getBattle_current_index() {
        return battle_current_index;
    }

    public void setBattle_current_index(int battle_current_index) {
        this.battle_current_index = battle_current_index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLeague_id() {
        return league_id;
    }

    public void setLeague_id(String league_id) {
        this.league_id = league_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getBattle_duration() {
        return battle_duration;
    }

    public void setBattle_duration(int battle_duration) {
        this.battle_duration = battle_duration;
    }

    public List<TeamPlayerStatsBean> getTeam_b_player_stats() {
        return team_b_player_stats;
    }

    public void setTeam_b_player_stats(List<TeamPlayerStatsBean> team_b_player_stats) {
        this.team_b_player_stats = team_b_player_stats;
    }

    public List<TeamPlayerStatsBean> getTeam_a_player_stats() {
        return team_a_player_stats;
    }

    public void setTeam_a_player_stats(List<TeamPlayerStatsBean> team_a_player_stats) {
        this.team_a_player_stats = team_a_player_stats;
    }

    public static class TeamPlayerStatsBean {
        /**
         * game_count : 10
         * damage_per_min : 471.512
         * kda : 2.941
         * golds_per_min : 402.095
         * player_name : Topoon
         * team_id : 877
         * position : 上单
         * avatar : https://qn.feijing88.com/feijing-home/egame/image/20190924/42c4c787965a4a029eedbba99ccebc54.png
         * hero_pick_stats : [{"hero_name_en":"Ornn","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20191111/a04d59c30e8046acaade95cca180e293.png","hero_name":"山隐之焰","hero_game_count":1,"hero_win_count":1,"hero_id":208},{"hero_name_en":"Gnar","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/e8e3567660164010ba60f75c00e6fed6.png","hero_name":"迷失之牙","hero_game_count":2,"hero_win_count":1,"hero_id":33},{"hero_name_en":"Cho'Gath","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190622/39f8c5c3750a4ff7ac37d47eeab868a2.png","hero_name":"虚空恐惧","hero_game_count":1,"hero_win_count":0,"hero_id":17},{"hero_name_en":"Aatrox","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20190621/355dbeb592aa46b5ac522cebfcf52aeb.png","hero_name":"暗裔剑魔","hero_game_count":1,"hero_win_count":0,"hero_id":1},{"hero_name_en":"Renekton","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/180c5a712c2242259fe9ef7b17c35a43.jpg","hero_name":"荒漠屠夫","hero_game_count":3,"hero_win_count":3,"hero_id":85},{"hero_name_en":"Camille","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/d3d8145e2cf948aa967aa9a11429396a.jpg","hero_name":"青钢影","hero_game_count":1,"hero_win_count":1,"hero_id":134},{"hero_name_en":"Gangplank","hero_logo":"https://qn.feijing88.com/feijing-home/egame/image/20181120/e15ccebb865e4b65af2d90b2df1ffc69.jpg","hero_name":"海洋之灾","hero_game_count":1,"hero_win_count":1,"hero_id":31}]
         * win_rate : 0.7
         * offered_rate : 0.553
         */

        private int game_count;
        private double damage_per_min;
        private double kda;
        private double golds_per_min;
        private String player_name;
        private String team_id;
        private String position;
        private String avatar;
        private double win_rate;
        private double offered_rate;
        private List<HeroPickStatsBeanX> hero_pick_stats;

        public int getGame_count() {
            return game_count;
        }

        public void setGame_count(int game_count) {
            this.game_count = game_count;
        }

        public double getDamage_per_min() {
            return damage_per_min;
        }

        public void setDamage_per_min(double damage_per_min) {
            this.damage_per_min = damage_per_min;
        }

        public double getKda() {
            return kda;
        }

        public void setKda(double kda) {
            this.kda = kda;
        }

        public double getGolds_per_min() {
            return golds_per_min;
        }

        public void setGolds_per_min(double golds_per_min) {
            this.golds_per_min = golds_per_min;
        }

        public String getPlayer_name() {
            return player_name;
        }

        public void setPlayer_name(String player_name) {
            this.player_name = player_name;
        }

        public String getTeam_id() {
            return team_id;
        }

        public void setTeam_id(String team_id) {
            this.team_id = team_id;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public double getWin_rate() {
            return win_rate;
        }

        public void setWin_rate(double win_rate) {
            this.win_rate = win_rate;
        }

        public double getOffered_rate() {
            return offered_rate;
        }

        public void setOffered_rate(double offered_rate) {
            this.offered_rate = offered_rate;
        }

        public List<HeroPickStatsBeanX> getHero_pick_stats() {
            return hero_pick_stats;
        }

        public void setHero_pick_stats(List<HeroPickStatsBeanX> hero_pick_stats) {
            this.hero_pick_stats = hero_pick_stats;
        }

        public static class HeroPickStatsBeanX {
            /**
             * hero_name_en : Ornn
             * hero_logo : https://qn.feijing88.com/feijing-home/egame/image/20191111/a04d59c30e8046acaade95cca180e293.png
             * hero_name : 山隐之焰
             * hero_game_count : 1
             * hero_win_count : 1
             * hero_id : 208
             */

            private String hero_name_en;
            private String hero_logo;
            private String hero_name;
            private int hero_game_count;
            private int hero_win_count;
            private int hero_id;

            public String getHero_name_en() {
                return hero_name_en;
            }

            public void setHero_name_en(String hero_name_en) {
                this.hero_name_en = hero_name_en;
            }

            public String getHero_logo() {
                return hero_logo;
            }

            public void setHero_logo(String hero_logo) {
                this.hero_logo = hero_logo;
            }

            public String getHero_name() {
                return hero_name;
            }

            public void setHero_name(String hero_name) {
                this.hero_name = hero_name;
            }

            public int getHero_game_count() {
                return hero_game_count;
            }

            public void setHero_game_count(int hero_game_count) {
                this.hero_game_count = hero_game_count;
            }

            public int getHero_win_count() {
                return hero_win_count;
            }

            public void setHero_win_count(int hero_win_count) {
                this.hero_win_count = hero_win_count;
            }

            public int getHero_id() {
                return hero_id;
            }

            public void setHero_id(int hero_id) {
                this.hero_id = hero_id;
            }
        }
    }
}
