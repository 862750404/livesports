package com.yunbao.live.activity;

import android.content.Intent;

import androidx.recyclerview.widget.GridLayoutManager;

import com.alibaba.fastjson.JSON;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.bean.MainRecommendBean;
import com.yunbao.common.custom.CommonRefreshView;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.interfaces.OnItemClickListener;
import com.yunbao.common.utils.AppLog;
import com.yunbao.live.R;
import com.yunbao.live.adapter.LiveRecommendAdapter;
import com.yunbao.live.http.LiveHttpUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Juwan
 * @date 2020/9/22
 * Description:
 */
public class LiveRecommendActivity extends AbsActivity {
    private LiveRecommendAdapter adapter;
    List<MainRecommendBean> mainRecommendBeanList = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_live_recommend;
    }


    @Override
    protected void main() {
        setRecommendList();
        setTitle("推荐直播");
    }

    private void setRecommendList() {

        CommonRefreshView mRefreshView = findViewById(R.id.live_refreshView_recommend);
        mRefreshView.setEmptyLayoutId(R.layout.view_no_data_default);
        mRefreshView.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRefreshView.setDataHelper(new CommonRefreshView.DataHelper<MainRecommendBean>() {
            @Override
            public RefreshAdapter<MainRecommendBean> getAdapter() {
                if (adapter == null) {
                    adapter = new LiveRecommendAdapter(mContext);
                    adapter.setOnItemClickListener(new OnItemClickListener<MainRecommendBean>() {
                        @Override
                        public void onItemClick(MainRecommendBean bean, int position) {
                            Intent intent = new Intent(mContext, LiveGameActivity.class);
                            intent.putExtra(Constants.LIVE_RECOMMEND_DATA, bean);
                            mContext.startActivity(intent);
                        }
                    });
                }
                return adapter;
            }

            @Override
            public void loadData(int p, HttpCallback callback) {
                if (p == 1) {
                    mainRecommendBeanList.clear();
                }
                LiveHttpUtil.getHot(p, 8, callback);
            }

            @Override
            public List<MainRecommendBean> processData(String[] info) {
                List<MainRecommendBean> mainRecommendBeans = JSON.parseArray(Arrays.toString(info), MainRecommendBean.class);
                if (mainRecommendBeanList != null && !mainRecommendBeanList.isEmpty()) {
                    for (int i = 0; i < mainRecommendBeans.size(); i++) {
                        for (int j = 0; j < mainRecommendBeanList.size(); j++) {
                            if (mainRecommendBeanList.get(j).getUid().equals(mainRecommendBeans.get(i).getUid())) {
                                mainRecommendBeans.remove(i);
                            }
                        }
                    }
                }
                if (mainRecommendBeanList != null) {
                    mainRecommendBeanList.addAll(mainRecommendBeans);
                }
                return mainRecommendBeans;
            }

            @Override
            public void onRefreshSuccess(List<MainRecommendBean> list, int listCount) {

            }

            @Override
            public void onRefreshFailure() {

            }

            @Override
            public void onLoadMoreSuccess(List<MainRecommendBean> loadItemList, int loadItemCount) {

            }

            @Override
            public void onLoadMoreFailure() {

            }
        });
        mRefreshView.initData();
    }
}
